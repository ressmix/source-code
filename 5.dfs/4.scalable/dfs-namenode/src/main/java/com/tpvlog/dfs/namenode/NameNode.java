package com.tpvlog.dfs.namenode;

import com.tpvlog.dfs.namenode.file.FSNameSystem;
import com.tpvlog.dfs.namenode.log.EditLogReplicator;
import com.tpvlog.dfs.namenode.log.FSImageUploadServer;
import com.tpvlog.dfs.namenode.register.DataNodeManager;
import com.tpvlog.dfs.namenode.server.NameNodeRpcServer;

/**
 * NameNode启动类
 *
 * @author Ressmix
 */
public class NameNode {

    // 负责管理集群元数据的核心组件，即内存文件目录树
    private FSNameSystem namesystem;

    // 负责管理集群DataNode的组件
    private DataNodeManager datanodeManager;

    // NameNode对外提供rpc服务的Server
    private NameNodeRpcServer rpcServer;

    // Edits Log同步组件
    private EditLogReplicator replicator;

    // fsimage同步组件
    private FSImageUploadServer fsimageUploadServer;

    public static void main(String[] args) throws Exception {
        NameNode namenode = new NameNode();
        namenode.init();
        namenode.start();
    }

    private void init() {
        this.namesystem = new FSNameSystem();
        this.datanodeManager = new DataNodeManager();
        this.replicator = new EditLogReplicator(namesystem);
        this.rpcServer = new NameNodeRpcServer(this.namesystem, this.datanodeManager, this.replicator);
        this.fsimageUploadServer = new FSImageUploadServer();
    }

    private void start() throws Exception {
        this.fsimageUploadServer.start();
        this.rpcServer.start();
        this.rpcServer.blockUntilShutdown();
    }
}

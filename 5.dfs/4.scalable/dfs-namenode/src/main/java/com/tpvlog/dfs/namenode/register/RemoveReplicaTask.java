package com.tpvlog.dfs.namenode.register;

/**
 * 文件删除任务
 */
public class RemoveReplicaTask {
    private String filename;
    private DataNodeInfo targetDataNode;

    public RemoveReplicaTask(String filename, DataNodeInfo targetDataNode) {
        this.filename = filename;
        this.targetDataNode = targetDataNode;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public DataNodeInfo getTargetDataNode() {
        return targetDataNode;
    }

    public void setTargetDataNode(DataNodeInfo targetDataNode) {
        this.targetDataNode = targetDataNode;
    }

}
package com.tpvlog.dfs.rpc.service;// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: NameNodeServiceProto.proto

public interface ShutdownResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:ShutdownResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>int32 status = 1;</code>
   * @return The status.
   */
  int getStatus();
}

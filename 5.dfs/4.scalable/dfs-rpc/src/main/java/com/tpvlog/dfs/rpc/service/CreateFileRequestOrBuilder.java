package com.tpvlog.dfs.rpc.service;// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: NameNodeServiceProto.proto

public interface CreateFileRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:CreateFileRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string filename = 1;</code>
   * @return The filename.
   */
  String getFilename();
  /**
   * <code>string filename = 1;</code>
   * @return The bytes for filename.
   */
  com.google.protobuf.ByteString
      getFilenameBytes();
}

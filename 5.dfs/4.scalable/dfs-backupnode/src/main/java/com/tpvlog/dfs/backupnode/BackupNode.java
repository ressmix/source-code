package com.tpvlog.dfs.backupnode;

import com.tpvlog.dfs.backupnode.file.FSNameSystem;
import com.tpvlog.dfs.backupnode.log.EditsLogFetcher;
import com.tpvlog.dfs.backupnode.log.FSImageCheckPointer;
import com.tpvlog.dfs.backupnode.server.NameNodeRpcClient;

/**
 * 启动类
 */
public class BackupNode {
    private volatile Boolean isRunning = true;

    private FSNameSystem namesystem;
    private NameNodeRpcClient rpcClient;

    public static void main(String[] args) {
        BackupNode backupNode = new BackupNode();
        backupNode.init();
        backupNode.start();
    }

    public void init() {
        this.namesystem = new FSNameSystem();
        this.rpcClient = new NameNodeRpcClient();
    }

    public void start() {
        // 定期拉取Edits Log线程
        EditsLogFetcher editsLogFetcher = new EditsLogFetcher(this, namesystem, rpcClient);
        editsLogFetcher.start();

        // 定期生成fsimage快照线程
        FSImageCheckPointer checkPointer = new FSImageCheckPointer(this, namesystem, rpcClient);
        checkPointer.start();
    }

    public Boolean isRunning() {
        return isRunning;
    }
}

package com.tpvlog.dfs.datanode.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.tpvlog.dfs.datanode.config.DataNodeConfig.DATA_DIR;

/**
 * DataNode元数据信息
 *
 * @author Ressmix
 */
public class DataNodeInfo {

    // 存储的文件列表
    private List<String> filenames = new ArrayList<String>();

    // 存储的文件总大小
    private Long storedDataSize = 0L;

    public List<String> getFilenames() {
        return filenames;
    }

    public void setFilenames(List<String> filenames) {
        this.filenames = filenames;
    }

    public Long getStoredDataSize() {
        return storedDataSize;
    }

    public void setStoredDataSize(Long storedDataSize) {
        this.storedDataSize = storedDataSize;
    }

    public void addFilename(String filename) {
        this.filenames.add(filename);
    }

    public void addStoredDataSize(Long storedDataSize) {
        this.storedDataSize += storedDataSize;
    }

    public static  String getAbsoluteFilename(String relativeFilename) throws Exception {
        String[] relativeFilenameSplited = relativeFilename.split("/");

        String dirPath = DATA_DIR;
        for (int i = 0; i < relativeFilenameSplited.length - 1; i++) {
            if (i == 0) {
                continue;
            }
            dirPath += "\\" + relativeFilenameSplited[i];
        }

        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String absoluteFilename = dirPath + "\\" +
                relativeFilenameSplited[relativeFilenameSplited.length - 1];
        return absoluteFilename;
    }

    @Override
    public String toString() {
        return "DataNodeInfo [filenames=" + filenames + ", storedDataSize=" + storedDataSize + "]";
    }
}

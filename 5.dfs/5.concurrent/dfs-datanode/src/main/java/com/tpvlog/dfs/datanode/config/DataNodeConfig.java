package com.tpvlog.dfs.datanode.config;

/**
 * DataNode配置类
 *
 * @author Ressmix
 */
public class DataNodeConfig {
    public static final String NAMENODE_HOSTNAME = "localhost";
    public static final Integer NAMENODE_PORT = 50070;

    public static final String DATANODE_HOSTNAME = "localhost";
    public static final String DATANODE_IP = "127.0.0.1";

    public static final Integer NIO_PORT = 9301;

    public static final String DATA_DIR = "C:\\Users\\Ressmix\\Desktop\\data";

}

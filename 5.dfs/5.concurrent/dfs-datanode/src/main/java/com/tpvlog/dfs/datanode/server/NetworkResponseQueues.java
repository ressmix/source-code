package com.tpvlog.dfs.datanode.server;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 响应队列
 *
 * @author Ressmix
 */
public class NetworkResponseQueues {

    private NetworkResponseQueues() {
    }

    // KEY为Processor标识,每个Processor线程对应一个响应队列
    private Map<Integer, ConcurrentLinkedQueue<NetworkResponse>> responseQueues = new HashMap<>();

    public void assignQueue(Integer processorId) {
        ConcurrentLinkedQueue<NetworkResponse> queue = new ConcurrentLinkedQueue<>();
        responseQueues.put(processorId, queue);
    }

    private static class InstanceHolder {
        private static final NetworkResponseQueues instance = new NetworkResponseQueues();
    }

    public static NetworkResponseQueues getInstance() {
        return InstanceHolder.instance;
    }

    // 添加一个响应
    public void offer(Integer processorId, NetworkResponse response) {
        responseQueues.get(processorId).offer(response);
    }

    // 获取一个响应
    public NetworkResponse poll(Integer processorId) {
        return responseQueues.get(processorId).poll();
    }
}

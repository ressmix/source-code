package com.tpvlog.dfs.datanode.server;

import java.nio.ByteBuffer;

public class NetworkResponse {

    private String client;

    private ByteBuffer buffer;

    public ByteBuffer getBuffer() {
        return buffer;
    }

    public void setBuffer(ByteBuffer buffer) {
        this.buffer = buffer;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

}

package com.tpvlog.dfs.datanode.server;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 全局请求队列
 *
 * @author Ressmix
 */
public class NetworkRequestQueue {

    private final ConcurrentLinkedQueue<NetworkRequest> requestQueue = new ConcurrentLinkedQueue<>();

    private NetworkRequestQueue() {
    }

    private static class InstanceHolder {
        private static final NetworkRequestQueue instance = new NetworkRequestQueue();
    }

    public static NetworkRequestQueue getInstance() {
        return InstanceHolder.instance;
    }

    public void offer(NetworkRequest request) {
        requestQueue.offer(request);
    }

    public NetworkRequest poll() {
        return requestQueue.poll();
    }

}


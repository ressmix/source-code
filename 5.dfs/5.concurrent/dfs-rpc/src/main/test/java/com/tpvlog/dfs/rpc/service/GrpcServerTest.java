package com.tpvlog.dfs.rpc.service;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;

public class GrpcServerTest {
    private final int port = 19080;
    private Server server;

    public static void main(String[] args) throws IOException, InterruptedException {
        final GrpcServerTest grpcServer = new GrpcServerTest();
        grpcServer.start();
        grpcServer.blockUntilShutdown();
    }

    private void start() throws IOException {
        server = ServerBuilder.forPort(port).addService(new NameNodeServiceImpl()).build().start();
        System.out.println("------- NameNodeService Started -------");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.err.println("------shutting down gRPC server since JVM is shutting down-------");
                GrpcServerTest.this.stop();
                System.err.println("------server shut down------");
            }
        });
    }

    /**
     * 服务接口实现类
     */
    private class NameNodeServiceImpl extends NameNodeServiceGrpc.NameNodeServiceImplBase {
        public void register(RegisterRequest request, StreamObserver<RegisterResponse> responseObserver) {
            RegisterResponse response = RegisterResponse.newBuilder().setStatus(200).build();
            // onNext()方法向客户端返回结果
            responseObserver.onNext(response);
            // 告诉客户端这次调用已经完成
            responseObserver.onCompleted();
        }

        public void heartbeat(HeartbeatRequest request, StreamObserver<HeartbeatResponse> responseObserver) {
            HeartbeatResponse response=HeartbeatResponse.newBuilder().setStatus(200).build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }
}

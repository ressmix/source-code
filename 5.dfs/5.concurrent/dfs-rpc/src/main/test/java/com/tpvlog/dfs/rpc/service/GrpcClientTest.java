package com.tpvlog.dfs.rpc.service;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.concurrent.TimeUnit;

public class GrpcClientTest {
    private final ManagedChannel channel;

    private final NameNodeServiceGrpc.NameNodeServiceBlockingStub blockingStub;

    private static final String host = "127.0.0.1";

    private static final int ip = 19080;

    public GrpcClientTest(String host, int port) {
        // usePlaintext表示明文传输，否则需要配置ssl
        // channel表示通信通道
        channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
        // 存根
        blockingStub = NameNodeServiceGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public void testHeartbeat(String name) {
        HeartbeatRequest request = HeartbeatRequest.newBuilder().setIp("127.0.0.1").setHostname("localhost").build();
        HeartbeatResponse response = blockingStub.heartbeat(request);
        System.out.println(name + ": " + response.getStatus());
    }

    public void testRegister(String name) {
        RegisterRequest request = RegisterRequest.newBuilder().setIp("127.0.0.1").setHostname("localhost").build();
        RegisterResponse response = blockingStub.register(request);
        System.out.println(response.getStatus());
    }

    public static void main(String[] args) {
        GrpcClientTest client = new GrpcClientTest(host, ip);
        for (int i = 0; i <= 5; i++) {
            client.testHeartbeat("<<<<<Heartbeat result>>>>>-" + i);
        }

        for (int i = 0; i <= 5; i++) {
            client.testHeartbeat("<<<<<Register result>>>>>-" + i);
        }
    }
}

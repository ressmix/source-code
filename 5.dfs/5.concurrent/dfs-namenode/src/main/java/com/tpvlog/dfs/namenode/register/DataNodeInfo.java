package com.tpvlog.dfs.namenode.register;

import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 用来描述datanode的信息
 *
 * @author Ressmix
 */
public class DataNodeInfo implements Comparable<DataNodeInfo> {

    /**
     * ip地址
     */
    private String ip;
    /**
     * 机器名字
     */
    private String hostname;
    /**
     * NIO端口
     */
    private int nioPort;
    /**
     * 最近一次心跳的时间
     */
    private long latestHeartbeatTime = System.currentTimeMillis();
    /**
     * 已经存储数据的大小
     */
    private long storedDataSize;

    /**
     * 副本复制任务队列
     */
    private ConcurrentLinkedQueue<ReplicateTask> replicateTaskQueue = new ConcurrentLinkedQueue<>();

    /**
     * 删除副本任务队列
     */
    private ConcurrentLinkedQueue<RemoveReplicaTask> removeReplicaTaskQueue = new ConcurrentLinkedQueue<>();


    public DataNodeInfo(String ip, String hostname) {
        this.ip = ip;
        this.hostname = hostname;
    }

    public DataNodeInfo(String ip, String hostname, int nioPort) {
        this.ip = ip;
        this.hostname = hostname;
        this.nioPort = nioPort;
        this.latestHeartbeatTime = System.currentTimeMillis();
        this.storedDataSize = 0L;
    }

    public String getId() {
        return this.ip + "-" + this.hostname;
    }

    public void addStoredDataSize(long storedDataSize) {
        this.storedDataSize += storedDataSize;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getNioPort() {
        return nioPort;
    }

    public void setNioPort(int nioPort) {
        this.nioPort = nioPort;
    }

    public long getLatestHeartbeatTime() {
        return latestHeartbeatTime;
    }

    public void setLatestHeartbeatTime(long latestHeartbeatTime) {
        this.latestHeartbeatTime = latestHeartbeatTime;
    }

    public long getStoredDataSize() {
        return storedDataSize;
    }

    public void setStoredDataSize(long storedDataSize) {
        this.storedDataSize = storedDataSize;
    }

    @Override
    public int compareTo(DataNodeInfo o) {
        if (this.storedDataSize - o.getStoredDataSize() > 0) {
            return 1;
        } else if (this.storedDataSize - o.getStoredDataSize() < 0) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataNodeInfo that = (DataNodeInfo) o;
        return ip.equals(that.ip) &&
                hostname.equals(that.hostname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, hostname);
    }

    @Override
    public String toString() {
        return "DataNodeInfo [ip=" + ip + ", hostname=" + hostname + ", latestHeartbeatTime=" + latestHeartbeatTime
                + ", storedDataSize=" + storedDataSize + "]";
    }

    public void addReplicateTask(ReplicateTask replicateTask) {
        replicateTaskQueue.offer(replicateTask);
    }

    public ReplicateTask pollReplicateTask() {
        if (!replicateTaskQueue.isEmpty()) {
            return replicateTaskQueue.poll();
        }
        return null;
    }

    public void addRemoveReplicaTask(RemoveReplicaTask removeReplicaTask) {
        removeReplicaTaskQueue.offer(removeReplicaTask);
    }

    public RemoveReplicaTask pollRemoveReplicaTask() {
        if (!removeReplicaTaskQueue.isEmpty()) {
            return removeReplicaTaskQueue.poll();
        }
        return null;
    }
}

package com.tpvlog.dfs.backupnode.log;

/**
 * CheckPoint
 *
 * @author Ressmix
 */
public class CheckPoint {

    // 最近一次checkpoint时间点
    private long checkpointTime = System.currentTimeMillis();

    // 最近一次checkpoint对应的目录树的最大txid
    private long syncedTxid = -1L;

    // 最近一次checkpoint中fsimage文件名
    private String fsimageFile = "";

    public long getCheckpointTime() {
        return checkpointTime;
    }

    public void setCheckpointTime(long checkpointTime) {
        this.checkpointTime = checkpointTime;
    }

    public long getSyncedTxid() {
        return syncedTxid;
    }

    public void setSyncedTxid(long syncedTxid) {
        this.syncedTxid = syncedTxid;
    }

    public String getFsimageFile() {
        return fsimageFile;
    }

    public void setFsimageFile(String fsimageFile) {
        this.fsimageFile = fsimageFile;
    }
}

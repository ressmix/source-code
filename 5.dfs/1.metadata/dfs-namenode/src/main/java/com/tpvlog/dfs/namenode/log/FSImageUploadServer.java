package com.tpvlog.dfs.namenode.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

/**
 * fsimage server
 *
 * @author Ressmix
 */
public class FSImageUploadServer extends Thread {

    private Selector selector;

    public FSImageUploadServer() {
        this.init();
    }

    private void init() {
        ServerSocketChannel serverSocketChannel = null;
        try {
            selector = Selector.open();
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.socket().bind(new InetSocketAddress(9000), 100);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("FSImageUploadServer启动，监听9000端口......");

        while (true) {
            try {
                selector.select();
                Iterator<SelectionKey> keysIterator = selector.selectedKeys().iterator();
                while (keysIterator.hasNext()) {
                    SelectionKey key = (SelectionKey) keysIterator.next();
                    keysIterator.remove();
                    try {
                        handleRequest(key);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }

    private void handleRequest(SelectionKey key) throws IOException {
        // 1.建立连接
        if (key.isAcceptable()) {
            handleConnectRequest(key);
        }
        // 2.接收到请求
        else if (key.isReadable()) {
            handleReadableRequest(key);
        }
        // 3.发送响应
        else if (key.isWritable()) {
            handleWritableRequest(key);
        }
    }

    /**
     * 处理BackupNode连接请求
     */
    private void handleConnectRequest(SelectionKey key) throws IOException {
        SocketChannel channel = null;
        try {
            ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
            channel = serverSocketChannel.accept();
            if (channel != null) {
                channel.configureBlocking(false);
                channel.register(selector, SelectionKey.OP_READ);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (channel != null) {
                channel.close();
            }
        }
    }

    /**
     * 处理发送fsimage文件的请求
     */
    private void handleReadableRequest(SelectionKey key) throws IOException {
        SocketChannel socketChannel = null;
        try {
            String fsimageFile = "C:\\Users\\Ressmix\\Desktop\\editslog\\fsimage.meta";
            RandomAccessFile raf = null;
            FileOutputStream os = null;
            FileChannel fileChannel = null;

            try {
                socketChannel = (SocketChannel) key.channel();
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                int total = 0;
                int count = -1;
                if ((count = socketChannel.read(buffer)) > 0) {
                    File file = new File(fsimageFile);
                    if (file.exists()) {
                        file.delete();
                    }

                    raf = new RandomAccessFile(fsimageFile, "rw");
                    os = new FileOutputStream(raf.getFD());
                    fileChannel = os.getChannel();

                    total += count;
                    buffer.flip();
                    fileChannel.write(buffer);
                    buffer.clear();
                } else {
                    socketChannel.close();
                }

                while ((count = socketChannel.read(buffer)) > 0) {
                    total += count;
                    buffer.flip();
                    fileChannel.write(buffer);
                    buffer.clear();
                }

                if (total > 0) {
                    System.out.println("接收fsimage文件以及写入本地磁盘完毕......");
                    fileChannel.force(false);
                    socketChannel.register(selector, SelectionKey.OP_WRITE);
                }
            } finally {
                if (os != null) {
                    os.close();
                }
                if (raf != null) {
                    raf.close();
                }
                if (fileChannel != null) {
                    fileChannel.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (socketChannel != null) {
                socketChannel.close();
            }
        }
    }

    /**
     * 处理返回响应给BackupNode
     */
    private void handleWritableRequest(SelectionKey key) throws IOException {
        SocketChannel channel = null;
        try {
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            buffer.put("SUCCESS".getBytes());
            buffer.flip();
            channel = (SocketChannel) key.channel();
            channel.write(buffer);
            System.out.println("fsimage上传完毕，返回响应SUCCESS给backupnode......");
            channel.register(selector, SelectionKey.OP_READ);
        } catch (Exception e) {
            e.printStackTrace();
            if (channel != null) {
                channel.close();
            }
        }
    }
}

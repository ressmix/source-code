package com.tpvlog.dfs.namenode.server;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tpvlog.dfs.namenode.file.FSNameSystem;
import com.tpvlog.dfs.namenode.log.EditLogReplicator;
import com.tpvlog.dfs.namenode.register.DataNodeManager;
import com.tpvlog.dfs.rpc.service.*;
import io.grpc.stub.StreamObserver;

import java.util.List;

/**
 * NameNode的RPC服务接口
 *
 * @author Ressmix
 */
public class NameNodeServiceImpl extends NameNodeServiceGrpc.NameNodeServiceImplBase {

    public static final Integer STATUS_SUCCESS = 1;
    public static final Integer STATUS_FAILURE = 2;
    public static final Integer STATUS_SHUTDOWN = 3;

    // 负责管理元数据的核心组件
    private FSNameSystem namesystem;

    // 负责管理集群中所有的datanode的组件
    private DataNodeManager datanodeManager;

    // 负责日志同步的组件
    private EditLogReplicator replicator;

    private volatile Boolean isRunning = true;

    public NameNodeServiceImpl(FSNameSystem namesystem, DataNodeManager datanodeManager, EditLogReplicator replicator) {
        this.namesystem = namesystem;
        this.datanodeManager = datanodeManager;
        this.replicator = replicator;
    }

    /**
     * DataNode注册
     */
    public void register(RegisterRequest request, StreamObserver<RegisterResponse> responseObserver) {
        // 使用DataNodeManager组件完成DataNode注册
        datanodeManager.register(request.getIp(), request.getHostname());

        RegisterResponse response = RegisterResponse.newBuilder().setStatus(STATUS_SUCCESS).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    /**
     * DataNode心跳
     */
    public void heartbeat(HeartbeatRequest request, StreamObserver<HeartbeatResponse> responseObserver) {
        // 使用DataNodeManager组件完成DataNode心跳
        datanodeManager.heartbeat(request.getIp(), request.getHostname());

        HeartbeatResponse response = HeartbeatResponse.newBuilder().setStatus(STATUS_SUCCESS).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    /**
     * 创建目录
     */
    @Override
    public void mkdir(MkDirRequest request, StreamObserver<MkDirResponse> responseObserver) {
        try {
            MkDirResponse response = null;
            if (!isRunning) {
                response = MkDirResponse.newBuilder().setStatus(STATUS_SHUTDOWN).build();
            } else {
                this.namesystem.mkdir(request.getPath());
                response = MkDirResponse.newBuilder().setStatus(STATUS_SUCCESS).build();
            }
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 优雅停机
     */
    @Override
    public void shutdown(ShutdownRequest request, StreamObserver<ShutdownResponse> responseObserver) {
        isRunning = false;
        namesystem.flush();
        namesystem.saveCheckpointTxid();
        System.out.println("优雅关闭namenode......");
    }

    /**
     * Edits Log日志同步
     */
    @Override
    public void fetchEditsLog(FetchEditsLogRequest request, StreamObserver<FetchEditsLogResponse> responseObserver) {
        if(!isRunning) {
            FetchEditsLogResponse response = FetchEditsLogResponse.newBuilder()
                    .setEditsLog(new JSONArray().toJSONString())
                    .build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
            return;
        }

        // 委托组件EditLogReplicator完成Edits Log日志拉取
        long syncedTxid = request.getSyncedTxid();
        List<String> list = replicator.fetchEditsLog(syncedTxid);
        String result = JSONObject.toJSONString(list);
        FetchEditsLogResponse response = FetchEditsLogResponse.newBuilder().setEditsLog(result).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    /**
     * 更新checkpoint txid
     */
    @Override
    public void updateCheckpointTxid(UpdateCheckpointTxidRequest request,
                                     StreamObserver<UpdateCheckpointTxidResponse> responseObserver) {
        long txid = request.getTxid();
        namesystem.setCheckpointTxid(txid);
        UpdateCheckpointTxidResponse response = UpdateCheckpointTxidResponse.newBuilder() .setStatus(1).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}

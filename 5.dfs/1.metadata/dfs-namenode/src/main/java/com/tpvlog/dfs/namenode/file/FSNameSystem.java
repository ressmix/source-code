package com.tpvlog.dfs.namenode.file;

import com.alibaba.fastjson.JSONObject;
import com.tpvlog.dfs.namenode.log.EditLogCleaner;
import com.tpvlog.dfs.namenode.log.FSEditlog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 负责管理元数据的核心组件
 *
 * @author Ressmix
 */
public class FSNameSystem {

    // 负责管理内存文件目录树的组件
    private FSDirectory directory;

    // 负责管理edits log写入磁盘的组件
    private FSEditlog editlog;

    // 负责清理Edits log的组件
    private EditLogCleaner editLogCleaner;

    // 最近一次checkpoint对应的txid
    private long checkpointTxid = 0L;

    public FSNameSystem() {
        this.directory = new FSDirectory();
        this.editlog = new FSEditlog();
        this.editLogCleaner = new EditLogCleaner(this);
        editLogCleaner.start();
        recoverNamespace();
    }

    /**
     * 创建目录
     *
     * @param path 目录路径
     * @return 是否成功
     */
    public Boolean mkdir(String path) throws Exception {
        this.directory.mkdir(path);
        this.editlog.logEdit("{'OP':'MKDIR','PATH':'" + path + "'}");
        return true;
    }

    /**
     * 强制把内存里的edits log刷入磁盘中
     */
    public void flush() {
        this.editlog.flush();
    }

    /**
     * 基于fsimage快照和edits log恢复元数据
     */
    public void recoverNamespace() {
        try {
            // 加载fsimage
            loadFSImage();
            // 加载最近的chekpoint
            loadCheckpointTxid();
            // 回放edits log
            loadEditLog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 基于fsimage快照生成内存文件目录树
     */
    private void loadFSImage() throws Exception {
        FileInputStream in = null;
        FileChannel channel = null;
        try {
            // 这里可以基于配置文件读取
            String path = "C:\\Users\\Ressmix\\Desktop\\editslog\\fsimage.meta";
            File file = new File(path);
            if (!file.exists()) {
                System.out.println("fsimage文件当前不存在，不进行恢复.......");
                return;
            }

            in = new FileInputStream(path);
            channel = in.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(1024 * 1024);
            int count = channel.read(buffer);

            buffer.flip();
            String fsimageJson = new String(buffer.array(), 0, count);
            System.out.println("恢复fsimage文件中的数据：" + fsimageJson);

            FSDirectory.INode dirTree = JSONObject.parseObject(fsimageJson, FSDirectory.INode.class);
            directory.setDirTree(dirTree);
        } finally {
            if (in != null) {
                in.close();
            }
            if (channel != null) {
                channel.close();
            }
        }
    }

    /**
     * 加载checkpoint txid
     */
    private void loadCheckpointTxid() throws Exception {
        FileInputStream in = null;
        FileChannel channel = null;
        try {
            String path = "C:\\Users\\Ressmix\\Desktop\\editslog\\checkpoint-txid.meta";
            File file = new File(path);
            if (!file.exists()) {
                System.out.println("checkpoint txid文件不存在，不进行恢复.......");
                return;
            }

            in = new FileInputStream(path);
            channel = in.getChannel();

            ByteBuffer buffer = ByteBuffer.allocate(1024);
            int count = channel.read(buffer);

            buffer.flip();
            long checkpointTxid = Long.valueOf(new String(buffer.array(), 0, count));
            System.out.println("恢复checkpoint txid：" + checkpointTxid);

            this.checkpointTxid = checkpointTxid;
        } finally {
            if (in != null) {
                in.close();
            }
            if (channel != null) {
                channel.close();
            }
        }
    }

    /**
     * 加载和回放editlog
     */
    private void loadEditLog() throws Exception {
        // 这里可以基于配置文件读取
        File dir = new File("C:\\Users\\Ressmix\\Desktop\\editslog\\");
        if (!dir.exists() || dir.listFiles() == null) {
            return;
        }

        List<File> files = new ArrayList<>();
        for (File file : dir.listFiles()) {
            if (file.getName().contains("edits")) {
                files.add(file);
            }
        }

        // 按edits log的文件名从小到大排序
        Collections.sort(files, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                Integer o1StartTxid = Integer.valueOf(o1.getName().split("-")[1]);
                Integer o2StartTxid = Integer.valueOf(o2.getName().split("-")[1]);
                return o1StartTxid - o2StartTxid;
            }
        });

        if (files == null || files.size() == 0) {
            System.out.println("当前没有任何editlog文件，不进行恢复......");
            return;
        }

        for (File file : files) {
            if (file.getName().contains("edits")) {
                System.out.println("准备恢复editlog文件中的数据：" + file.getName());

                String[] splitedName = file.getName().split("-");
                long startTxid = Long.valueOf(splitedName[1]);
                long endTxid = Long.valueOf(splitedName[2].split("[.]")[0]);

                // 如果是checkpointTxid之后的那些editlog都要加载出来
                if (endTxid > checkpointTxid) {
                    String currentEditsLogFile = "C:\\Users\\Ressmix\\Desktop\\editslog\\edits-"
                            + startTxid + "-" + endTxid + ".log";
                    List<String> editsLogs = Files.readAllLines(Paths.get(currentEditsLogFile),
                            StandardCharsets.UTF_8);

                    for (String editLogJson : editsLogs) {
                        JSONObject editLog = JSONObject.parseObject(editLogJson);
                        long txid = editLog.getLongValue("txid");

                        if (txid > checkpointTxid) {
                            System.out.println("准备回放editlog：" + editLogJson);
                            // 回放到内存里去
                            String op = editLog.getString("OP");
                            if (op.equals("MKDIR")) {
                                String path = editLog.getString("PATH");
                                directory.mkdir(path);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 将checkpoint txid保存到磁盘上去
     */
    public void saveCheckpointTxid() {
        String path = "C:\\Users\\Ressmix\\Desktop\\editslog\\checkpoint-txid.meta";

        RandomAccessFile raf = null;
        FileOutputStream out = null;
        FileChannel channel = null;

        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }

            ByteBuffer buffer = ByteBuffer.wrap(String.valueOf(checkpointTxid).getBytes());
            raf = new RandomAccessFile(path, "rw");
            out = new FileOutputStream(raf.getFD());
            channel = out.getChannel();

            channel.write(buffer);
            channel.force(false);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (raf != null) {
                    raf.close();
                }
                if (channel != null) {
                    channel.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public FSEditlog getEditsLog() {
        return editlog;
    }

    public void setCheckpointTxid(long txid) {
        System.out.println("接收到checkpoint txid：" + txid);
        this.checkpointTxid = txid;
    }

    public long getCheckpointTxid() {
        return checkpointTxid;
    }
}

package com.tpvlog.dfs.client;

/**
 * 作为文件系统的接口
 *
 * @author Ressmix
 */
public interface FileSystem {

    /**
     * 创建目录
     *
     * @param path 目录对应的路径
     * @throws Exception
     */
    void mkdir(String path) throws Exception;

    /**
     * 上传一个文件
     *
     * @param file     file byte array
     * @param filename 文件名，以如如文件分隔符开头，则移除
     * @param fileSize file size
     */
    Boolean upload(byte[] file, String filename, long fileSize) throws Exception;


    /**
     * 下载一个文件
     *
     * @param filename 文件名
     * @return 文件的字节数组
     * @throws Exception
     */
    byte[] download(String filename) throws Exception;

    /**
     * 优雅关闭
     *
     * @throws Exception
     */
    void shutdown() throws Exception;
}

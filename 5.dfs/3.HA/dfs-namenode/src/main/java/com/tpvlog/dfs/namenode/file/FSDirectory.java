package com.tpvlog.dfs.namenode.file;

import java.util.LinkedList;
import java.util.List;

/**
 * 管理文件目录树的核心组件
 *
 * @author Ressmix
 */
public class FSDirectory {
    // 内存中的文件目录树
    private INode dirTree;

    public FSDirectory() {
        // 初始化时只有根节点
        this.dirTree = new INode("/");
    }

    /**
     * 创建目录
     *
     * @param path 目录路径, eg: path = /usr/warehouse/hive
     */
    public void mkdir(String path) {
        synchronized (dirTree) {
            // eg: /usr/warehouse/hive -> ["","usr","warehosue","hive"]
            String[] pathes = path.split("/");
            INode parent = dirTree;

            for (String splitedPath : pathes) {
                if (splitedPath.trim().equals("")) {
                    continue;
                }

                INode dir = findDirectory(parent, splitedPath);
                if (dir != null) {
                    parent = dir;
                    continue;
                }

                INode child = new INode(splitedPath);
                parent.addChild(child);
                parent = child;
            }
        }
        // printDirTree(dirTree, "");
    }

    public Boolean createFile(String filename) {
        synchronized (dirTree) {
            String[] splitedFilename = filename.split("/");
            String realFilename = splitedFilename[splitedFilename.length - 1];

            INode parent = dirTree;
            for (int i = 0; i < splitedFilename.length - 1; i++) {
                if (i == 0) {
                    continue;
                }

                INode dir = findDirectory(parent, splitedFilename[i]);
                if (dir != null) {
                    parent = dir;
                    continue;
                }

                INode child = new INode(splitedFilename[i]);
                parent.addChild(child);
                parent = child;
            }

            if (existFile(parent, realFilename)) {
                return false;
            }

            // 创建文件
            INode file = new INode(realFilename);
            parent.addChild(file);
            System.out.println(dirTree);

            return true;
        }
    }

    public INode getDirTree() {
        return dirTree;
    }

    public void setDirTree(INode dirTree) {
        this.dirTree = dirTree;
    }

    /**
     * 代表文件目录树中的一个目录
     */
    public static class INode {
        private String path;
        private List<INode> children;

        public INode() {
        }

        public INode(String path) {
            this.path = path;
            this.children = new LinkedList<INode>();
        }

        public void addChild(INode inode) {
            this.children.add(inode);
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public List<INode> getChildren() {
            return children;
        }

        public void setChildren(List<INode> children) {
            this.children = children;
        }

        @Override
        public String toString() {
            return "INode [path=" + path + ", children=" + children + "]";
        }
    }


    /*---------------------------------------------PRIVATE METHOD-------------------------------------------------*/

    private void printDirTree(INode dirTree, String blank) {
        if (dirTree.getChildren().size() == 0) {
            return;
        }
        for (INode dir : dirTree.getChildren()) {
            System.out.println(blank + ((INode) dir).getPath());
            printDirTree((INode) dir, blank + " ");
        }
    }

    /**
     * 查找子目录
     * <p>
     * 在dir目录下查找子目录path
     */
    private INode findDirectory(INode dir, String path) {
        if (dir.getChildren().size() == 0) {
            return null;
        }

        for (INode child : dir.getChildren()) {
            if (child instanceof INode) {
                INode childDir = (INode) child;
                if ((childDir.getPath().equals(path))) {
                    return childDir;
                }
            }
        }
        return null;
    }

    /**
     * 目录下是否存在这个文件
     *
     * @param dir
     * @param filename
     * @return
     */
    private Boolean existFile(INode dir, String filename) {
        if (dir.getChildren() != null && dir.getChildren().size() > 0) {
            for (INode child : dir.getChildren()) {
                if (child.getPath().equals(filename)) {
                    return true;
                }
            }
        }
        return false;
    }
}

package com.tpvlog.dfs.rpc.service;// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: NameNodeServiceProto.proto

public final class NameNodeServiceProto {
  private NameNodeServiceProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_RegisterRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_RegisterRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_RegisterResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_RegisterResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_HeartbeatRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_HeartbeatRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_HeartbeatResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_HeartbeatResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_MkDirRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_MkDirRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_MkDirResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_MkDirResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ShutdownRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ShutdownRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ShutdownResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ShutdownResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_FetchEditsLogRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_FetchEditsLogRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_FetchEditsLogResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_FetchEditsLogResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_UpdateCheckpointTxidRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_UpdateCheckpointTxidRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_UpdateCheckpointTxidResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_UpdateCheckpointTxidResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_CreateFileRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_CreateFileRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_CreateFileResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_CreateFileResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_AllocateDataNodesRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_AllocateDataNodesRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_AllocateDataNodesResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_AllocateDataNodesResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_GetDataNodeForFileRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_GetDataNodeForFileRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_GetDataNodeForFileResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_GetDataNodeForFileResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_FullyReportRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_FullyReportRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_FullyReportResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_FullyReportResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_DeltaReportRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_DeltaReportRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_DeltaReportResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_DeltaReportResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\032NameNodeServiceProto.proto\"@\n\017Register" +
      "Request\022\n\n\002ip\030\001 \001(\t\022\020\n\010hostname\030\002 \001(\t\022\017\n" +
      "\007nioPort\030\003 \001(\005\"\"\n\020RegisterResponse\022\016\n\006st" +
      "atus\030\001 \001(\005\"A\n\020HeartbeatRequest\022\n\n\002ip\030\001 \001" +
      "(\t\022\020\n\010hostname\030\002 \001(\t\022\017\n\007nioPort\030\003 \001(\005\"5\n" +
      "\021HeartbeatResponse\022\016\n\006status\030\001 \001(\005\022\020\n\010co" +
      "mmands\030\002 \001(\t\"\034\n\014MkDirRequest\022\014\n\004path\030\001 \001" +
      "(\t\"\037\n\rMkDirResponse\022\016\n\006status\030\001 \001(\005\"\037\n\017S" +
      "hutdownRequest\022\014\n\004code\030\001 \001(\005\"\"\n\020Shutdown" +
      "Response\022\016\n\006status\030\001 \001(\005\"*\n\024FetchEditsLo" +
      "gRequest\022\022\n\nsyncedTxid\030\001 \001(\003\")\n\025FetchEdi" +
      "tsLogResponse\022\020\n\010editsLog\030\001 \001(\t\"+\n\033Updat" +
      "eCheckpointTxidRequest\022\014\n\004txid\030\001 \001(\003\".\n\034" +
      "UpdateCheckpointTxidResponse\022\016\n\006status\030\001" +
      " \001(\005\"%\n\021CreateFileRequest\022\020\n\010filename\030\001 " +
      "\001(\t\"$\n\022CreateFileResponse\022\016\n\006status\030\001 \001(" +
      "\005\"Z\n\030AllocateDataNodesRequest\022\020\n\010filenam" +
      "e\030\001 \001(\t\022\020\n\010filesize\030\002 \001(\003\022\032\n\022excludedDat" +
      "aNodeId\030\003 \001(\t\">\n\031AllocateDataNodesRespon" +
      "se\022\016\n\006status\030\001 \001(\005\022\021\n\tdatanodes\030\002 \001(\t\"I\n" +
      "\031GetDataNodeForFileRequest\022\020\n\010filename\030\001" +
      " \001(\t\022\032\n\022excludedDataNodeId\030\002 \001(\t\">\n\032GetD" +
      "ataNodeForFileResponse\022\016\n\006status\030\001 \001(\005\022\020" +
      "\n\010datanode\030\002 \001(\t\"`\n\022FullyReportRequest\022\024" +
      "\n\014filenameList\030\001 \001(\t\022\026\n\016storedDataSize\030\002" +
      " \001(\003\022\020\n\010hostname\030\003 \001(\t\022\n\n\002ip\030\004 \001(\t\"%\n\023Fu" +
      "llyReportResponse\022\016\n\006status\030\001 \001(\005\"V\n\022Del" +
      "taReportRequest\022\020\n\010filename\030\001 \001(\t\022\020\n\010fil" +
      "esize\030\002 \001(\003\022\020\n\010hostname\030\003 \001(\t\022\n\n\002ip\030\004 \001(" +
      "\t\"%\n\023DeltaReportResponse\022\016\n\006status\030\001 \001(\005" +
      "2\330\005\n\017NameNodeService\0221\n\010register\022\020.Regis" +
      "terRequest\032\021.RegisterResponse\"\000\0224\n\theart" +
      "beat\022\021.HeartbeatRequest\032\022.HeartbeatRespo" +
      "nse\"\000\022(\n\005mkdir\022\r.MkDirRequest\032\016.MkDirRes" +
      "ponse\"\000\0221\n\010shutdown\022\020.ShutdownRequest\032\021." +
      "ShutdownResponse\"\000\022@\n\rfetchEditsLog\022\025.Fe" +
      "tchEditsLogRequest\032\026.FetchEditsLogRespon" +
      "se\"\000\022U\n\024updateCheckpointTxid\022\034.UpdateChe" +
      "ckpointTxidRequest\032\035.UpdateCheckpointTxi" +
      "dResponse\"\000\0227\n\ncreateFile\022\022.CreateFileRe" +
      "quest\032\023.CreateFileResponse\"\000\022L\n\021allocate" +
      "DataNodes\022\031.AllocateDataNodesRequest\032\032.A" +
      "llocateDataNodesResponse\"\000\022O\n\022getDataNod" +
      "eForFile\022\032.GetDataNodeForFileRequest\032\033.G" +
      "etDataNodeForFileResponse\"\000\022F\n\027fullyRepo" +
      "rtDataNodeInfo\022\023.FullyReportRequest\032\024.Fu" +
      "llyReportResponse\"\000\022F\n\027deltaReportDataNo" +
      "deInfo\022\023.DeltaReportRequest\032\024.DeltaRepor" +
      "tResponse\"\000B\030B\024NameNodeServiceProtoP\001b\006p" +
      "roto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_RegisterRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_RegisterRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_RegisterRequest_descriptor,
        new String[] { "Ip", "Hostname", "NioPort", });
    internal_static_RegisterResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_RegisterResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_RegisterResponse_descriptor,
        new String[] { "Status", });
    internal_static_HeartbeatRequest_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_HeartbeatRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_HeartbeatRequest_descriptor,
        new String[] { "Ip", "Hostname", "NioPort", });
    internal_static_HeartbeatResponse_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_HeartbeatResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_HeartbeatResponse_descriptor,
        new String[] { "Status", "Commands", });
    internal_static_MkDirRequest_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_MkDirRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_MkDirRequest_descriptor,
        new String[] { "Path", });
    internal_static_MkDirResponse_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_MkDirResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_MkDirResponse_descriptor,
        new String[] { "Status", });
    internal_static_ShutdownRequest_descriptor =
      getDescriptor().getMessageTypes().get(6);
    internal_static_ShutdownRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ShutdownRequest_descriptor,
        new String[] { "Code", });
    internal_static_ShutdownResponse_descriptor =
      getDescriptor().getMessageTypes().get(7);
    internal_static_ShutdownResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ShutdownResponse_descriptor,
        new String[] { "Status", });
    internal_static_FetchEditsLogRequest_descriptor =
      getDescriptor().getMessageTypes().get(8);
    internal_static_FetchEditsLogRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_FetchEditsLogRequest_descriptor,
        new String[] { "SyncedTxid", });
    internal_static_FetchEditsLogResponse_descriptor =
      getDescriptor().getMessageTypes().get(9);
    internal_static_FetchEditsLogResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_FetchEditsLogResponse_descriptor,
        new String[] { "EditsLog", });
    internal_static_UpdateCheckpointTxidRequest_descriptor =
      getDescriptor().getMessageTypes().get(10);
    internal_static_UpdateCheckpointTxidRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_UpdateCheckpointTxidRequest_descriptor,
        new String[] { "Txid", });
    internal_static_UpdateCheckpointTxidResponse_descriptor =
      getDescriptor().getMessageTypes().get(11);
    internal_static_UpdateCheckpointTxidResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_UpdateCheckpointTxidResponse_descriptor,
        new String[] { "Status", });
    internal_static_CreateFileRequest_descriptor =
      getDescriptor().getMessageTypes().get(12);
    internal_static_CreateFileRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_CreateFileRequest_descriptor,
        new String[] { "Filename", });
    internal_static_CreateFileResponse_descriptor =
      getDescriptor().getMessageTypes().get(13);
    internal_static_CreateFileResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_CreateFileResponse_descriptor,
        new String[] { "Status", });
    internal_static_AllocateDataNodesRequest_descriptor =
      getDescriptor().getMessageTypes().get(14);
    internal_static_AllocateDataNodesRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_AllocateDataNodesRequest_descriptor,
        new String[] { "Filename", "Filesize", "ExcludedDataNodeId", });
    internal_static_AllocateDataNodesResponse_descriptor =
      getDescriptor().getMessageTypes().get(15);
    internal_static_AllocateDataNodesResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_AllocateDataNodesResponse_descriptor,
        new String[] { "Status", "Datanodes", });
    internal_static_GetDataNodeForFileRequest_descriptor =
      getDescriptor().getMessageTypes().get(16);
    internal_static_GetDataNodeForFileRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_GetDataNodeForFileRequest_descriptor,
        new String[] { "Filename", "ExcludedDataNodeId", });
    internal_static_GetDataNodeForFileResponse_descriptor =
      getDescriptor().getMessageTypes().get(17);
    internal_static_GetDataNodeForFileResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_GetDataNodeForFileResponse_descriptor,
        new String[] { "Status", "Datanode", });
    internal_static_FullyReportRequest_descriptor =
      getDescriptor().getMessageTypes().get(18);
    internal_static_FullyReportRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_FullyReportRequest_descriptor,
        new String[] { "FilenameList", "StoredDataSize", "Hostname", "Ip", });
    internal_static_FullyReportResponse_descriptor =
      getDescriptor().getMessageTypes().get(19);
    internal_static_FullyReportResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_FullyReportResponse_descriptor,
        new String[] { "Status", });
    internal_static_DeltaReportRequest_descriptor =
      getDescriptor().getMessageTypes().get(20);
    internal_static_DeltaReportRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_DeltaReportRequest_descriptor,
        new String[] { "Filename", "Filesize", "Hostname", "Ip", });
    internal_static_DeltaReportResponse_descriptor =
      getDescriptor().getMessageTypes().get(21);
    internal_static_DeltaReportResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_DeltaReportResponse_descriptor,
        new String[] { "Status", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}

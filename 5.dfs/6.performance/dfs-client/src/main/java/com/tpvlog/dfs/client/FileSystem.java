package com.tpvlog.dfs.client;

import com.tpvlog.dfs.client.req.FileInfo;
import com.tpvlog.dfs.client.resp.ResponseCallback;

/**
 * 作为文件系统的接口
 *
 * @author Ressmix
 */
public interface FileSystem {

    /**
     * 创建目录
     *
     * @param path 目录对应的路径
     * @throws Exception
     */
    void mkdir(String path) throws Exception;

    /**
     * 上传一个文件
     */
    Boolean upload(FileInfo fileInfo, ResponseCallback callback) throws Exception;

    /**
     * 下载一个文件
     */
    byte[] download(FileInfo fileInfo) throws Exception;

    /**
     * 优雅关闭
     *
     * @throws Exception
     */
    void shutdown() throws Exception;
}

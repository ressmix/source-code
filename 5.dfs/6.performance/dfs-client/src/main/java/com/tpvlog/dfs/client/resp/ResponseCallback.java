package com.tpvlog.dfs.client.resp;

/**
 * 响应回调函数接口
 */
public interface ResponseCallback {

    /**
     * 处理响应结果
     */
    void process(NetworkResponse response);
}

package com.tpvlog.dfs.client.req;

import java.util.Objects;

/**
 * 主机抽象
 */
public class Host {
    private final String hostname;
    private final String ip;
    private final Integer nioPort;

    public Host(String hostname, String ip, Integer nioPort) {
        this.hostname = hostname;
        this.ip = ip;
        this.nioPort = nioPort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Host host = (Host) o;
        return hostname.equals(host.hostname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hostname);
    }

    @Override
    public String toString() {
        return "Host{" +
                "hostname='" + hostname + '\'' +
                ", ip='" + ip + '\'' +
                ", nioPort=" + nioPort +
                '}';
    }

    public String getHostname() {
        return hostname;
    }

    public String getIp() {
        return ip;
    }

    public Integer getNioPort() {
        return nioPort;
    }
}

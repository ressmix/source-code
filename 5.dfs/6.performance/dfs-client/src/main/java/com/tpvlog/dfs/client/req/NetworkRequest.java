package com.tpvlog.dfs.client.req;

import com.tpvlog.dfs.client.resp.ResponseCallback;

import java.nio.ByteBuffer;

/**
 * 网络请求
 */
public class NetworkRequest {
    // 请求类型：4字节
    public static final Integer REQUEST_TYPE = 4;
    // 文件名大小：4字节
    public static final Integer FILENAME_LENGTH = 4;
    // 文件内容大小：4字节
    public static final Integer FILE_LENGTH = 8;
    // 上传文件请求
    public static final Integer REQUEST_SEND_FILE = 1;
    // 下载文件请求
    public static final Integer REQUEST_READ_FILE = 2;

    private String id;
    private String hostname;
    private String ip;
    private Integer nioPort;

    private Integer requestType;
    private Long sendTime;
    // 完整请求内容Buffer
    private ByteBuffer buffer;
    // 当前请求是否需要响应，当为true时，客户端必须等收到响应后，才会发送下一个请求
    private Boolean needResponse;

    private ResponseCallback callback;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getNioPort() {
        return nioPort;
    }

    public void setNioPort(Integer nioPort) {
        this.nioPort = nioPort;
    }

    public Integer getRequestType() {
        return requestType;
    }

    public void setRequestType(Integer requestType) {
        this.requestType = requestType;
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }

    public ByteBuffer getBuffer() {
        return buffer;
    }

    public void setBuffer(ByteBuffer buffer) {
        this.buffer = buffer;
    }

    public Boolean getNeedResponse() {
        return needResponse;
    }

    public void setNeedResponse(Boolean needResponse) {
        this.needResponse = needResponse;
    }

    public ResponseCallback getCallback() {
        return callback;
    }

    public void setCallback(ResponseCallback callback) {
        this.callback = callback;
    }
}
package com.tpvlog.dfs.client.resp;

import java.nio.ByteBuffer;

/**
 * 网络响应
 */
public class NetworkResponse {
	
	public static final String RESPONSE_SUCCESS = "SUCCESS";

	private String requestId;
	private String hostname;
	private String ip;
	private Integer nioPort;

	// 响应头大小
	private ByteBuffer lengthBuffer;
	// 响应体内容
	private ByteBuffer buffer;

	private Boolean error;
	private Boolean finished;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public ByteBuffer getLengthBuffer() {
		return lengthBuffer;
	}

	public void setLengthBuffer(ByteBuffer lengthBuffer) {
		this.lengthBuffer = lengthBuffer;
	}

	public ByteBuffer getBuffer() {
		return buffer;
	}

	public void setBuffer(ByteBuffer buffer) {
		this.buffer = buffer;
	}

	public Boolean getError() {
		return error;
	}

	public void setError(Boolean error) {
		this.error = error;
	}

	public Boolean getFinished() {
		return finished;
	}

	public void setFinished(Boolean finished) {
		this.finished = finished;
	}

	public Integer getNioPort() {
		return nioPort;
	}

	public void setNioPort(Integer nioPort) {
		this.nioPort = nioPort;
	}
}

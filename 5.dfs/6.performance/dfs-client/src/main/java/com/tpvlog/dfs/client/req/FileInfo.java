package com.tpvlog.dfs.client.req;

/**
 * 文件信息
 */
public class FileInfo {
    // 文件名
    private String filename;
    // 文件内容大小
    private Integer fileLength;
    // 文件内容
    private byte[] file;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Integer getFileLength() {
        return fileLength;
    }

    public void setFileLength(Integer fileLength) {
        this.fileLength = fileLength;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

}

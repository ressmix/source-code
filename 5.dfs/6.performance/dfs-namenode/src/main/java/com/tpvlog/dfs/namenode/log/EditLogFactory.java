package com.tpvlog.dfs.namenode.log;

/**
 * 生成editlog内容的工厂类
 *
 * @author Ressmix
 */
public class EditLogFactory {

    public static String mkdir(String path) {
        return "{'OP':'MKDIR','PATH':'" + path + "'}";
    }

    public static String createFile(String path) {
        return "{'OP':'CREATE','PATH':'" + path + "'}";
    }

}

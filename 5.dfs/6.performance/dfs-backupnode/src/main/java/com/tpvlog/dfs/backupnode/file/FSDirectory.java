package com.tpvlog.dfs.backupnode.file;

import com.alibaba.fastjson.JSONObject;
import com.tpvlog.dfs.backupnode.log.FSImage;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 管理文件目录树的核心组件
 *
 * @author Ressmix
 */
public class FSDirectory {
    // 内存中的文件目录树
    private INode dirTree;

    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    // 当前文件目录树已经更新到的最新txid
    private long maxTxid = 0L;

    public void writeLock() {
        lock.writeLock().lock();
    }

    public void writeUnlock() {
        lock.writeLock().unlock();
    }

    public void readLock() {
        lock.readLock().lock();
    }

    public void readUnlock() {
        lock.readLock().unlock();
    }

    public FSDirectory() {
        // 初始化时只有根节点
        this.dirTree = new INode("/");
    }

    /**
     * 以json格式获取到fsimage内存元数据
     */
    public FSImage getFSImage() {
        FSImage fsimage = null;
        try {
            readLock();
            String fsimageJson = JSONObject.toJSONString(dirTree);
            long lastedTxid = this.maxTxid;
            fsimage = new FSImage(lastedTxid, fsimageJson);
        } finally {
            readUnlock();
        }
        return fsimage;
    }

    /**
     * 创建目录
     *
     * @param path 目录路径, eg: path = /usr/warehouse/hive
     */
    public void mkdir(Long txid, String path) {
        try {
            writeLock();
            // 记录最新edits log的txid
            maxTxid = txid;

            // eg: /usr/warehouse/hive -> ["","usr","warehosue","hive"]
            String[] pathes = path.split("/");
            INode parent = dirTree;

            for (String splitedPath : pathes) {
                if (splitedPath.trim().equals("")) {
                    continue;
                }

                INode dir = findDirectory(parent, splitedPath); // parent="/usr"
                if (dir != null) {
                    parent = dir;
                    continue;
                }

                INode child = new INode(splitedPath);
                parent.addChild(child);
                parent = child;
            }
        } finally {
            writeUnlock();
        }
    }

    /**
     * 创建文件
     *
     * @param filename 文件名
     * @return
     */
    public Boolean ceateFile(long txid, String filename) {
        try {
            writeLock();

            maxTxid = txid;
            String[] splitedFilename = filename.split("/");
            String realFilename = splitedFilename[splitedFilename.length - 1];

            INode parent = dirTree;
            for (int i = 0; i < splitedFilename.length - 1; i++) {
                if (i == 0) {
                    continue;
                }

                INode dir = findDirectory(parent, splitedFilename[i]);
                if (dir != null) {
                    parent = dir;
                    continue;
                }

                INode child = new INode(splitedFilename[i]);
                parent.addChild(child);
                parent = child;
            }

            if (existFile(parent, realFilename)) {
                return false;
            }

            INode file = new INode(realFilename);
            parent.addChild(file);
            return true;
        } finally {
            writeUnlock();
        }
    }

    /*---------------------------------------------PRIVATE METHPD----------------------------------------------------*/

    private void printDirTree(INode dirTree, String blank) {
        if (dirTree.getChildren().size() == 0) {
            return;
        }
        for (INode dir : dirTree.getChildren()) {
            System.out.println(blank + ((INode) dir).getPath());
            printDirTree((INode) dir, blank + " ");
        }
    }

    /**
     * 查找子目录
     * <p>
     * 在dir目录下查找子目录path
     */
    private INode findDirectory(INode dir, String path) {
        if (dir.getChildren().size() == 0) {
            return null;
        }

        for (INode child : dir.getChildren()) {
            if (child instanceof INode) {
                INode childDir = (INode) child;
                if ((childDir.getPath().equals(path))) {
                    return childDir;
                }
            }
        }
        return null;
    }

    /**
     * 目录下是否存在这个文件
     *
     * @param dir
     * @param filename
     * @return
     */
    private Boolean existFile(INode dir, String filename) {
        if (dir.getChildren() != null && dir.getChildren().size() > 0) {
            for (INode child : dir.getChildren()) {
                if (child.getPath().equals(filename)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 代表文件目录树中的一个目录
     */
    public static class INode {
        private String path;
        private List<INode> children;

        public INode() {
        }

        public INode(String path) {
            this.path = path;
            this.children = new LinkedList<INode>();
        }

        public void addChild(INode inode) {
            this.children.add(inode);
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public List<INode> getChildren() {
            return children;
        }

        public void setChildren(List<INode> children) {
            this.children = children;
        }

        @Override
        public String toString() {
            return "INode [path=" + path + ", children=" + children + "]";
        }
    }

    public INode getDirTree() {
        return dirTree;
    }

    public void setDirTree(INode dirTree) {
        this.dirTree = dirTree;
    }

    public long getMaxTxid() {
        return maxTxid;
    }

    public void setMaxTxid(long maxTxid) {
        this.maxTxid = maxTxid;
    }
}

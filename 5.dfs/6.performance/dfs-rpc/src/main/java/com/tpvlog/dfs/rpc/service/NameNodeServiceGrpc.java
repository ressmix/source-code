package com.tpvlog.dfs.rpc.service;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.35.0)",
    comments = "Source: NameNodeServiceProto.proto")
public final class NameNodeServiceGrpc {

  private NameNodeServiceGrpc() {}

  public static final String SERVICE_NAME = "NameNodeService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<RegisterRequest,
      RegisterResponse> getRegisterMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "register",
      requestType = RegisterRequest.class,
      responseType = RegisterResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<RegisterRequest,
      RegisterResponse> getRegisterMethod() {
    io.grpc.MethodDescriptor<RegisterRequest, RegisterResponse> getRegisterMethod;
    if ((getRegisterMethod = NameNodeServiceGrpc.getRegisterMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getRegisterMethod = NameNodeServiceGrpc.getRegisterMethod) == null) {
          NameNodeServiceGrpc.getRegisterMethod = getRegisterMethod =
              io.grpc.MethodDescriptor.<RegisterRequest, RegisterResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "register"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  RegisterRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  RegisterResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("register"))
              .build();
        }
      }
    }
    return getRegisterMethod;
  }

  private static volatile io.grpc.MethodDescriptor<HeartbeatRequest,
      HeartbeatResponse> getHeartbeatMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "heartbeat",
      requestType = HeartbeatRequest.class,
      responseType = HeartbeatResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<HeartbeatRequest,
      HeartbeatResponse> getHeartbeatMethod() {
    io.grpc.MethodDescriptor<HeartbeatRequest, HeartbeatResponse> getHeartbeatMethod;
    if ((getHeartbeatMethod = NameNodeServiceGrpc.getHeartbeatMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getHeartbeatMethod = NameNodeServiceGrpc.getHeartbeatMethod) == null) {
          NameNodeServiceGrpc.getHeartbeatMethod = getHeartbeatMethod =
              io.grpc.MethodDescriptor.<HeartbeatRequest, HeartbeatResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "heartbeat"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  HeartbeatRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  HeartbeatResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("heartbeat"))
              .build();
        }
      }
    }
    return getHeartbeatMethod;
  }

  private static volatile io.grpc.MethodDescriptor<MkDirRequest,
      MkDirResponse> getMkdirMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "mkdir",
      requestType = MkDirRequest.class,
      responseType = MkDirResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<MkDirRequest,
      MkDirResponse> getMkdirMethod() {
    io.grpc.MethodDescriptor<MkDirRequest, MkDirResponse> getMkdirMethod;
    if ((getMkdirMethod = NameNodeServiceGrpc.getMkdirMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getMkdirMethod = NameNodeServiceGrpc.getMkdirMethod) == null) {
          NameNodeServiceGrpc.getMkdirMethod = getMkdirMethod =
              io.grpc.MethodDescriptor.<MkDirRequest, MkDirResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "mkdir"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  MkDirRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  MkDirResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("mkdir"))
              .build();
        }
      }
    }
    return getMkdirMethod;
  }

  private static volatile io.grpc.MethodDescriptor<ShutdownRequest,
      ShutdownResponse> getShutdownMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "shutdown",
      requestType = ShutdownRequest.class,
      responseType = ShutdownResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ShutdownRequest,
      ShutdownResponse> getShutdownMethod() {
    io.grpc.MethodDescriptor<ShutdownRequest, ShutdownResponse> getShutdownMethod;
    if ((getShutdownMethod = NameNodeServiceGrpc.getShutdownMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getShutdownMethod = NameNodeServiceGrpc.getShutdownMethod) == null) {
          NameNodeServiceGrpc.getShutdownMethod = getShutdownMethod =
              io.grpc.MethodDescriptor.<ShutdownRequest, ShutdownResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "shutdown"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ShutdownRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ShutdownResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("shutdown"))
              .build();
        }
      }
    }
    return getShutdownMethod;
  }

  private static volatile io.grpc.MethodDescriptor<FetchEditsLogRequest,
      FetchEditsLogResponse> getFetchEditsLogMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "fetchEditsLog",
      requestType = FetchEditsLogRequest.class,
      responseType = FetchEditsLogResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<FetchEditsLogRequest,
      FetchEditsLogResponse> getFetchEditsLogMethod() {
    io.grpc.MethodDescriptor<FetchEditsLogRequest, FetchEditsLogResponse> getFetchEditsLogMethod;
    if ((getFetchEditsLogMethod = NameNodeServiceGrpc.getFetchEditsLogMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getFetchEditsLogMethod = NameNodeServiceGrpc.getFetchEditsLogMethod) == null) {
          NameNodeServiceGrpc.getFetchEditsLogMethod = getFetchEditsLogMethod =
              io.grpc.MethodDescriptor.<FetchEditsLogRequest, FetchEditsLogResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "fetchEditsLog"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  FetchEditsLogRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  FetchEditsLogResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("fetchEditsLog"))
              .build();
        }
      }
    }
    return getFetchEditsLogMethod;
  }

  private static volatile io.grpc.MethodDescriptor<UpdateCheckpointTxidRequest,
      UpdateCheckpointTxidResponse> getUpdateCheckpointTxidMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "updateCheckpointTxid",
      requestType = UpdateCheckpointTxidRequest.class,
      responseType = UpdateCheckpointTxidResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<UpdateCheckpointTxidRequest,
      UpdateCheckpointTxidResponse> getUpdateCheckpointTxidMethod() {
    io.grpc.MethodDescriptor<UpdateCheckpointTxidRequest, UpdateCheckpointTxidResponse> getUpdateCheckpointTxidMethod;
    if ((getUpdateCheckpointTxidMethod = NameNodeServiceGrpc.getUpdateCheckpointTxidMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getUpdateCheckpointTxidMethod = NameNodeServiceGrpc.getUpdateCheckpointTxidMethod) == null) {
          NameNodeServiceGrpc.getUpdateCheckpointTxidMethod = getUpdateCheckpointTxidMethod =
              io.grpc.MethodDescriptor.<UpdateCheckpointTxidRequest, UpdateCheckpointTxidResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "updateCheckpointTxid"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  UpdateCheckpointTxidRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  UpdateCheckpointTxidResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("updateCheckpointTxid"))
              .build();
        }
      }
    }
    return getUpdateCheckpointTxidMethod;
  }

  private static volatile io.grpc.MethodDescriptor<CreateFileRequest,
      CreateFileResponse> getCreateFileMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "createFile",
      requestType = CreateFileRequest.class,
      responseType = CreateFileResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<CreateFileRequest,
      CreateFileResponse> getCreateFileMethod() {
    io.grpc.MethodDescriptor<CreateFileRequest, CreateFileResponse> getCreateFileMethod;
    if ((getCreateFileMethod = NameNodeServiceGrpc.getCreateFileMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getCreateFileMethod = NameNodeServiceGrpc.getCreateFileMethod) == null) {
          NameNodeServiceGrpc.getCreateFileMethod = getCreateFileMethod =
              io.grpc.MethodDescriptor.<CreateFileRequest, CreateFileResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "createFile"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  CreateFileRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  CreateFileResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("createFile"))
              .build();
        }
      }
    }
    return getCreateFileMethod;
  }

  private static volatile io.grpc.MethodDescriptor<AllocateDataNodesRequest,
      AllocateDataNodesResponse> getAllocateDataNodesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "allocateDataNodes",
      requestType = AllocateDataNodesRequest.class,
      responseType = AllocateDataNodesResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<AllocateDataNodesRequest,
      AllocateDataNodesResponse> getAllocateDataNodesMethod() {
    io.grpc.MethodDescriptor<AllocateDataNodesRequest, AllocateDataNodesResponse> getAllocateDataNodesMethod;
    if ((getAllocateDataNodesMethod = NameNodeServiceGrpc.getAllocateDataNodesMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getAllocateDataNodesMethod = NameNodeServiceGrpc.getAllocateDataNodesMethod) == null) {
          NameNodeServiceGrpc.getAllocateDataNodesMethod = getAllocateDataNodesMethod =
              io.grpc.MethodDescriptor.<AllocateDataNodesRequest, AllocateDataNodesResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "allocateDataNodes"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  AllocateDataNodesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  AllocateDataNodesResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("allocateDataNodes"))
              .build();
        }
      }
    }
    return getAllocateDataNodesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<GetDataNodeForFileRequest,
      GetDataNodeForFileResponse> getGetDataNodeForFileMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getDataNodeForFile",
      requestType = GetDataNodeForFileRequest.class,
      responseType = GetDataNodeForFileResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<GetDataNodeForFileRequest,
      GetDataNodeForFileResponse> getGetDataNodeForFileMethod() {
    io.grpc.MethodDescriptor<GetDataNodeForFileRequest, GetDataNodeForFileResponse> getGetDataNodeForFileMethod;
    if ((getGetDataNodeForFileMethod = NameNodeServiceGrpc.getGetDataNodeForFileMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getGetDataNodeForFileMethod = NameNodeServiceGrpc.getGetDataNodeForFileMethod) == null) {
          NameNodeServiceGrpc.getGetDataNodeForFileMethod = getGetDataNodeForFileMethod =
              io.grpc.MethodDescriptor.<GetDataNodeForFileRequest, GetDataNodeForFileResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getDataNodeForFile"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  GetDataNodeForFileRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  GetDataNodeForFileResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("getDataNodeForFile"))
              .build();
        }
      }
    }
    return getGetDataNodeForFileMethod;
  }

  private static volatile io.grpc.MethodDescriptor<FullyReportRequest,
      FullyReportResponse> getFullyReportDataNodeInfoMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "fullyReportDataNodeInfo",
      requestType = FullyReportRequest.class,
      responseType = FullyReportResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<FullyReportRequest,
      FullyReportResponse> getFullyReportDataNodeInfoMethod() {
    io.grpc.MethodDescriptor<FullyReportRequest, FullyReportResponse> getFullyReportDataNodeInfoMethod;
    if ((getFullyReportDataNodeInfoMethod = NameNodeServiceGrpc.getFullyReportDataNodeInfoMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getFullyReportDataNodeInfoMethod = NameNodeServiceGrpc.getFullyReportDataNodeInfoMethod) == null) {
          NameNodeServiceGrpc.getFullyReportDataNodeInfoMethod = getFullyReportDataNodeInfoMethod =
              io.grpc.MethodDescriptor.<FullyReportRequest, FullyReportResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "fullyReportDataNodeInfo"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  FullyReportRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  FullyReportResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("fullyReportDataNodeInfo"))
              .build();
        }
      }
    }
    return getFullyReportDataNodeInfoMethod;
  }

  private static volatile io.grpc.MethodDescriptor<DeltaReportRequest,
      DeltaReportResponse> getDeltaReportDataNodeInfoMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "deltaReportDataNodeInfo",
      requestType = DeltaReportRequest.class,
      responseType = DeltaReportResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<DeltaReportRequest,
      DeltaReportResponse> getDeltaReportDataNodeInfoMethod() {
    io.grpc.MethodDescriptor<DeltaReportRequest, DeltaReportResponse> getDeltaReportDataNodeInfoMethod;
    if ((getDeltaReportDataNodeInfoMethod = NameNodeServiceGrpc.getDeltaReportDataNodeInfoMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getDeltaReportDataNodeInfoMethod = NameNodeServiceGrpc.getDeltaReportDataNodeInfoMethod) == null) {
          NameNodeServiceGrpc.getDeltaReportDataNodeInfoMethod = getDeltaReportDataNodeInfoMethod =
              io.grpc.MethodDescriptor.<DeltaReportRequest, DeltaReportResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "deltaReportDataNodeInfo"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  DeltaReportRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  DeltaReportResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("deltaReportDataNodeInfo"))
              .build();
        }
      }
    }
    return getDeltaReportDataNodeInfoMethod;
  }

  private static volatile io.grpc.MethodDescriptor<RebalanceRequest,
      RebalanceResponse> getRebalanceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "rebalance",
      requestType = RebalanceRequest.class,
      responseType = RebalanceResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<RebalanceRequest,
      RebalanceResponse> getRebalanceMethod() {
    io.grpc.MethodDescriptor<RebalanceRequest, RebalanceResponse> getRebalanceMethod;
    if ((getRebalanceMethod = NameNodeServiceGrpc.getRebalanceMethod) == null) {
      synchronized (NameNodeServiceGrpc.class) {
        if ((getRebalanceMethod = NameNodeServiceGrpc.getRebalanceMethod) == null) {
          NameNodeServiceGrpc.getRebalanceMethod = getRebalanceMethod =
              io.grpc.MethodDescriptor.<RebalanceRequest, RebalanceResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "rebalance"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  RebalanceRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  RebalanceResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NameNodeServiceMethodDescriptorSupplier("rebalance"))
              .build();
        }
      }
    }
    return getRebalanceMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static NameNodeServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<NameNodeServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<NameNodeServiceStub>() {
        @Override
        public NameNodeServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new NameNodeServiceStub(channel, callOptions);
        }
      };
    return NameNodeServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static NameNodeServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<NameNodeServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<NameNodeServiceBlockingStub>() {
        @Override
        public NameNodeServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new NameNodeServiceBlockingStub(channel, callOptions);
        }
      };
    return NameNodeServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static NameNodeServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<NameNodeServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<NameNodeServiceFutureStub>() {
        @Override
        public NameNodeServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new NameNodeServiceFutureStub(channel, callOptions);
        }
      };
    return NameNodeServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class NameNodeServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void register(RegisterRequest request,
        io.grpc.stub.StreamObserver<RegisterResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getRegisterMethod(), responseObserver);
    }

    /**
     */
    public void heartbeat(HeartbeatRequest request,
        io.grpc.stub.StreamObserver<HeartbeatResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getHeartbeatMethod(), responseObserver);
    }

    /**
     */
    public void mkdir(MkDirRequest request,
        io.grpc.stub.StreamObserver<MkDirResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getMkdirMethod(), responseObserver);
    }

    /**
     */
    public void shutdown(ShutdownRequest request,
        io.grpc.stub.StreamObserver<ShutdownResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getShutdownMethod(), responseObserver);
    }

    /**
     */
    public void fetchEditsLog(FetchEditsLogRequest request,
        io.grpc.stub.StreamObserver<FetchEditsLogResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFetchEditsLogMethod(), responseObserver);
    }

    /**
     */
    public void updateCheckpointTxid(UpdateCheckpointTxidRequest request,
        io.grpc.stub.StreamObserver<UpdateCheckpointTxidResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getUpdateCheckpointTxidMethod(), responseObserver);
    }

    /**
     */
    public void createFile(CreateFileRequest request,
        io.grpc.stub.StreamObserver<CreateFileResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getCreateFileMethod(), responseObserver);
    }

    /**
     */
    public void allocateDataNodes(AllocateDataNodesRequest request,
        io.grpc.stub.StreamObserver<AllocateDataNodesResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getAllocateDataNodesMethod(), responseObserver);
    }

    /**
     */
    public void getDataNodeForFile(GetDataNodeForFileRequest request,
        io.grpc.stub.StreamObserver<GetDataNodeForFileResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetDataNodeForFileMethod(), responseObserver);
    }

    /**
     */
    public void fullyReportDataNodeInfo(FullyReportRequest request,
        io.grpc.stub.StreamObserver<FullyReportResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFullyReportDataNodeInfoMethod(), responseObserver);
    }

    /**
     */
    public void deltaReportDataNodeInfo(DeltaReportRequest request,
        io.grpc.stub.StreamObserver<DeltaReportResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getDeltaReportDataNodeInfoMethod(), responseObserver);
    }

    /**
     */
    public void rebalance(RebalanceRequest request,
        io.grpc.stub.StreamObserver<RebalanceResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getRebalanceMethod(), responseObserver);
    }

    @Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getRegisterMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                RegisterRequest,
                RegisterResponse>(
                  this, METHODID_REGISTER)))
          .addMethod(
            getHeartbeatMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                HeartbeatRequest,
                HeartbeatResponse>(
                  this, METHODID_HEARTBEAT)))
          .addMethod(
            getMkdirMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                MkDirRequest,
                MkDirResponse>(
                  this, METHODID_MKDIR)))
          .addMethod(
            getShutdownMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                ShutdownRequest,
                ShutdownResponse>(
                  this, METHODID_SHUTDOWN)))
          .addMethod(
            getFetchEditsLogMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                FetchEditsLogRequest,
                FetchEditsLogResponse>(
                  this, METHODID_FETCH_EDITS_LOG)))
          .addMethod(
            getUpdateCheckpointTxidMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                UpdateCheckpointTxidRequest,
                UpdateCheckpointTxidResponse>(
                  this, METHODID_UPDATE_CHECKPOINT_TXID)))
          .addMethod(
            getCreateFileMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                CreateFileRequest,
                CreateFileResponse>(
                  this, METHODID_CREATE_FILE)))
          .addMethod(
            getAllocateDataNodesMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                AllocateDataNodesRequest,
                AllocateDataNodesResponse>(
                  this, METHODID_ALLOCATE_DATA_NODES)))
          .addMethod(
            getGetDataNodeForFileMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                GetDataNodeForFileRequest,
                GetDataNodeForFileResponse>(
                  this, METHODID_GET_DATA_NODE_FOR_FILE)))
          .addMethod(
            getFullyReportDataNodeInfoMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                FullyReportRequest,
                FullyReportResponse>(
                  this, METHODID_FULLY_REPORT_DATA_NODE_INFO)))
          .addMethod(
            getDeltaReportDataNodeInfoMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                DeltaReportRequest,
                DeltaReportResponse>(
                  this, METHODID_DELTA_REPORT_DATA_NODE_INFO)))
          .addMethod(
            getRebalanceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                RebalanceRequest,
                RebalanceResponse>(
                  this, METHODID_REBALANCE)))
          .build();
    }
  }

  /**
   */
  public static final class NameNodeServiceStub extends io.grpc.stub.AbstractAsyncStub<NameNodeServiceStub> {
    private NameNodeServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected NameNodeServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new NameNodeServiceStub(channel, callOptions);
    }

    /**
     */
    public void register(RegisterRequest request,
        io.grpc.stub.StreamObserver<RegisterResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getRegisterMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void heartbeat(HeartbeatRequest request,
        io.grpc.stub.StreamObserver<HeartbeatResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getHeartbeatMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void mkdir(MkDirRequest request,
        io.grpc.stub.StreamObserver<MkDirResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getMkdirMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void shutdown(ShutdownRequest request,
        io.grpc.stub.StreamObserver<ShutdownResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getShutdownMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void fetchEditsLog(FetchEditsLogRequest request,
        io.grpc.stub.StreamObserver<FetchEditsLogResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFetchEditsLogMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateCheckpointTxid(UpdateCheckpointTxidRequest request,
        io.grpc.stub.StreamObserver<UpdateCheckpointTxidResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getUpdateCheckpointTxidMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void createFile(CreateFileRequest request,
        io.grpc.stub.StreamObserver<CreateFileResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getCreateFileMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void allocateDataNodes(AllocateDataNodesRequest request,
        io.grpc.stub.StreamObserver<AllocateDataNodesResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getAllocateDataNodesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getDataNodeForFile(GetDataNodeForFileRequest request,
        io.grpc.stub.StreamObserver<GetDataNodeForFileResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetDataNodeForFileMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void fullyReportDataNodeInfo(FullyReportRequest request,
        io.grpc.stub.StreamObserver<FullyReportResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFullyReportDataNodeInfoMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deltaReportDataNodeInfo(DeltaReportRequest request,
        io.grpc.stub.StreamObserver<DeltaReportResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getDeltaReportDataNodeInfoMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void rebalance(RebalanceRequest request,
        io.grpc.stub.StreamObserver<RebalanceResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getRebalanceMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class NameNodeServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<NameNodeServiceBlockingStub> {
    private NameNodeServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected NameNodeServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new NameNodeServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public RegisterResponse register(RegisterRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getRegisterMethod(), getCallOptions(), request);
    }

    /**
     */
    public HeartbeatResponse heartbeat(HeartbeatRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getHeartbeatMethod(), getCallOptions(), request);
    }

    /**
     */
    public MkDirResponse mkdir(MkDirRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getMkdirMethod(), getCallOptions(), request);
    }

    /**
     */
    public ShutdownResponse shutdown(ShutdownRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getShutdownMethod(), getCallOptions(), request);
    }

    /**
     */
    public FetchEditsLogResponse fetchEditsLog(FetchEditsLogRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFetchEditsLogMethod(), getCallOptions(), request);
    }

    /**
     */
    public UpdateCheckpointTxidResponse updateCheckpointTxid(UpdateCheckpointTxidRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getUpdateCheckpointTxidMethod(), getCallOptions(), request);
    }

    /**
     */
    public CreateFileResponse createFile(CreateFileRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getCreateFileMethod(), getCallOptions(), request);
    }

    /**
     */
    public AllocateDataNodesResponse allocateDataNodes(AllocateDataNodesRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getAllocateDataNodesMethod(), getCallOptions(), request);
    }

    /**
     */
    public GetDataNodeForFileResponse getDataNodeForFile(GetDataNodeForFileRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetDataNodeForFileMethod(), getCallOptions(), request);
    }

    /**
     */
    public FullyReportResponse fullyReportDataNodeInfo(FullyReportRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFullyReportDataNodeInfoMethod(), getCallOptions(), request);
    }

    /**
     */
    public DeltaReportResponse deltaReportDataNodeInfo(DeltaReportRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getDeltaReportDataNodeInfoMethod(), getCallOptions(), request);
    }

    /**
     */
    public RebalanceResponse rebalance(RebalanceRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getRebalanceMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class NameNodeServiceFutureStub extends io.grpc.stub.AbstractFutureStub<NameNodeServiceFutureStub> {
    private NameNodeServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected NameNodeServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new NameNodeServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<RegisterResponse> register(
        RegisterRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getRegisterMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<HeartbeatResponse> heartbeat(
        HeartbeatRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getHeartbeatMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<MkDirResponse> mkdir(
        MkDirRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getMkdirMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ShutdownResponse> shutdown(
        ShutdownRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getShutdownMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<FetchEditsLogResponse> fetchEditsLog(
        FetchEditsLogRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFetchEditsLogMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<UpdateCheckpointTxidResponse> updateCheckpointTxid(
        UpdateCheckpointTxidRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getUpdateCheckpointTxidMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<CreateFileResponse> createFile(
        CreateFileRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getCreateFileMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<AllocateDataNodesResponse> allocateDataNodes(
        AllocateDataNodesRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getAllocateDataNodesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<GetDataNodeForFileResponse> getDataNodeForFile(
        GetDataNodeForFileRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetDataNodeForFileMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<FullyReportResponse> fullyReportDataNodeInfo(
        FullyReportRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFullyReportDataNodeInfoMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<DeltaReportResponse> deltaReportDataNodeInfo(
        DeltaReportRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getDeltaReportDataNodeInfoMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<RebalanceResponse> rebalance(
        RebalanceRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getRebalanceMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_REGISTER = 0;
  private static final int METHODID_HEARTBEAT = 1;
  private static final int METHODID_MKDIR = 2;
  private static final int METHODID_SHUTDOWN = 3;
  private static final int METHODID_FETCH_EDITS_LOG = 4;
  private static final int METHODID_UPDATE_CHECKPOINT_TXID = 5;
  private static final int METHODID_CREATE_FILE = 6;
  private static final int METHODID_ALLOCATE_DATA_NODES = 7;
  private static final int METHODID_GET_DATA_NODE_FOR_FILE = 8;
  private static final int METHODID_FULLY_REPORT_DATA_NODE_INFO = 9;
  private static final int METHODID_DELTA_REPORT_DATA_NODE_INFO = 10;
  private static final int METHODID_REBALANCE = 11;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final NameNodeServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(NameNodeServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_REGISTER:
          serviceImpl.register((RegisterRequest) request,
              (io.grpc.stub.StreamObserver<RegisterResponse>) responseObserver);
          break;
        case METHODID_HEARTBEAT:
          serviceImpl.heartbeat((HeartbeatRequest) request,
              (io.grpc.stub.StreamObserver<HeartbeatResponse>) responseObserver);
          break;
        case METHODID_MKDIR:
          serviceImpl.mkdir((MkDirRequest) request,
              (io.grpc.stub.StreamObserver<MkDirResponse>) responseObserver);
          break;
        case METHODID_SHUTDOWN:
          serviceImpl.shutdown((ShutdownRequest) request,
              (io.grpc.stub.StreamObserver<ShutdownResponse>) responseObserver);
          break;
        case METHODID_FETCH_EDITS_LOG:
          serviceImpl.fetchEditsLog((FetchEditsLogRequest) request,
              (io.grpc.stub.StreamObserver<FetchEditsLogResponse>) responseObserver);
          break;
        case METHODID_UPDATE_CHECKPOINT_TXID:
          serviceImpl.updateCheckpointTxid((UpdateCheckpointTxidRequest) request,
              (io.grpc.stub.StreamObserver<UpdateCheckpointTxidResponse>) responseObserver);
          break;
        case METHODID_CREATE_FILE:
          serviceImpl.createFile((CreateFileRequest) request,
              (io.grpc.stub.StreamObserver<CreateFileResponse>) responseObserver);
          break;
        case METHODID_ALLOCATE_DATA_NODES:
          serviceImpl.allocateDataNodes((AllocateDataNodesRequest) request,
              (io.grpc.stub.StreamObserver<AllocateDataNodesResponse>) responseObserver);
          break;
        case METHODID_GET_DATA_NODE_FOR_FILE:
          serviceImpl.getDataNodeForFile((GetDataNodeForFileRequest) request,
              (io.grpc.stub.StreamObserver<GetDataNodeForFileResponse>) responseObserver);
          break;
        case METHODID_FULLY_REPORT_DATA_NODE_INFO:
          serviceImpl.fullyReportDataNodeInfo((FullyReportRequest) request,
              (io.grpc.stub.StreamObserver<FullyReportResponse>) responseObserver);
          break;
        case METHODID_DELTA_REPORT_DATA_NODE_INFO:
          serviceImpl.deltaReportDataNodeInfo((DeltaReportRequest) request,
              (io.grpc.stub.StreamObserver<DeltaReportResponse>) responseObserver);
          break;
        case METHODID_REBALANCE:
          serviceImpl.rebalance((RebalanceRequest) request,
              (io.grpc.stub.StreamObserver<RebalanceResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @Override
    @SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class NameNodeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    NameNodeServiceBaseDescriptorSupplier() {}

    @Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return NameNodeServiceProto.getDescriptor();
    }

    @Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("NameNodeService");
    }
  }

  private static final class NameNodeServiceFileDescriptorSupplier
      extends NameNodeServiceBaseDescriptorSupplier {
    NameNodeServiceFileDescriptorSupplier() {}
  }

  private static final class NameNodeServiceMethodDescriptorSupplier
      extends NameNodeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    NameNodeServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (NameNodeServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new NameNodeServiceFileDescriptorSupplier())
              .addMethod(getRegisterMethod())
              .addMethod(getHeartbeatMethod())
              .addMethod(getMkdirMethod())
              .addMethod(getShutdownMethod())
              .addMethod(getFetchEditsLogMethod())
              .addMethod(getUpdateCheckpointTxidMethod())
              .addMethod(getCreateFileMethod())
              .addMethod(getAllocateDataNodesMethod())
              .addMethod(getGetDataNodeForFileMethod())
              .addMethod(getFullyReportDataNodeInfoMethod())
              .addMethod(getDeltaReportDataNodeInfoMethod())
              .addMethod(getRebalanceMethod())
              .build();
        }
      }
    }
    return result;
  }
}

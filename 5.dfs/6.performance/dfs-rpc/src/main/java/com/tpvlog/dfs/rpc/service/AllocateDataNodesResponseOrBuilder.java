package com.tpvlog.dfs.rpc.service;// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: NameNodeServiceProto.proto

public interface AllocateDataNodesResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:AllocateDataNodesResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>int32 status = 1;</code>
   * @return The status.
   */
  int getStatus();

  /**
   * <code>string datanodes = 2;</code>
   * @return The datanodes.
   */
  String getDatanodes();
  /**
   * <code>string datanodes = 2;</code>
   * @return The bytes for datanodes.
   */
  com.google.protobuf.ByteString
      getDatanodesBytes();
}

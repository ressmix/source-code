package com.tpvlog.seckill.operation.service;

import com.alibaba.fastjson.JSONObject;
import com.tpvlog.seckill.operation.dao.SeckillProductDAO;
import com.tpvlog.seckill.operation.domain.SeckillProduct;
import com.tpvlog.seckill.common.JedisManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

/**
 * 秒杀商品Service组件
 */
@Service
public class SeckillProductServiceImpl implements SeckillProductService {

    /**
     * 秒杀商品DAO组件
     */
    @Autowired
    private SeckillProductDAO seckillProductDAO;

    /**
     * 增加秒杀商品
     * @param seckillProduct 秒杀商品
     */
    public void add(SeckillProduct seckillProduct) {
        seckillProductDAO.add(seckillProduct);

        // 留一个作业：之前讲解了redis的各种数据结构的实战，大量的小案例的演示
        // 参考京东的秒杀页面，每一天有很多的场次，在redis里需要用一个数据结构维护一天的场次
        // 每一个场次是有很多的商品的，需要有一个数据结构维护一个场次内的很多商品
        // 这个应该怎么去做

        JedisManager jedisManager = JedisManager.getInstance();
        Jedis jedis = jedisManager.getJedis();
        jedis.lpush("seckill::products::" + seckillProduct.getSessionId(),
                JSONObject.toJSONString(seckillProduct));
    }

}

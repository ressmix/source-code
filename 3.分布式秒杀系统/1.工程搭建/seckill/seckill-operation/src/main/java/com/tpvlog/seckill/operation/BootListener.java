package com.tpvlog.seckill.operation;

import com.tpvlog.seckill.common.RocketMQProducer;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 系统启动监听器
 */
@Component
public class BootListener implements CommandLineRunner {

    @Override
    public void run(String... strings) throws Exception {
        RocketMQProducer.setProducerGroup("seckill-operation-producer-group");

        DefaultMQProducer producer = RocketMQProducer.getInstance().getProducer();

        Message message = new Message(
                "test_topic",
                null,
                ("Hello RocketMQ").getBytes(RemotingHelper.DEFAULT_CHARSET)
        );
        SendResult sendResult = producer.send(message);
        System.out.printf("%s%n", sendResult);
        System.out.println("系统启动时发送测试消息到RocketMQ成功......");
    }

}

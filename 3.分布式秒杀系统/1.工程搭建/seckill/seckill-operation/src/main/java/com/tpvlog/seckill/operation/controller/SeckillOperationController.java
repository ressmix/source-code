package com.tpvlog.seckill.operation.controller;

import com.alibaba.fastjson.JSONObject;
import com.tpvlog.seckill.common.RocketMQProducer;
import com.tpvlog.seckill.operation.domain.SeckillProduct;
import com.tpvlog.seckill.operation.domain.SeckillSession;
import com.tpvlog.seckill.operation.service.SeckillProductService;
import com.tpvlog.seckill.operation.service.SeckillSessionService;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 秒杀运营服务的接口
 */
@RestController
@RequestMapping("/seckill/operation")
public class SeckillOperationController {

    // 为了便于我们测试
    // 我这里会把所有的HTTP接口简化为GET方式
    // POST/PUT，还得用postman
    // 传递参数，也直接在GET请求后附加，不用JSON传递参数了

    /**
     * 秒杀场次Service组件
     */
    @Autowired
    private SeckillSessionService seckillSessionService;

    /**
     * 秒杀商品Service组件
     */
    @Autowired
    private SeckillProductService seckillProductService;

    /**
     * 增加秒杀场次的接口
     * @return
     */
    @GetMapping("/session/add")
    public String addSeckillSession(SeckillSession seckillSession) {
        seckillSessionService.add(seckillSession);
        return "success";
    }

    /**
     * 增加秒杀场次下商品的接口
     */
    @GetMapping("/product/add")
    public String addSeckillProduct(SeckillProduct seckillProduct) {
        seckillProductService.add(seckillProduct);

        DefaultMQProducer producer = RocketMQProducer.getInstance().getProducer();

        try {
            Message message = new Message(
                    "seckill_product_added_topic",
                    null,
                    JSONObject.toJSONString(seckillProduct).getBytes(RemotingHelper.DEFAULT_CHARSET)
            );
            SendResult sendResult = producer.send(message);
            System.out.printf("%s%n", sendResult);
            System.out.println("推送秒杀商品增加通知到MQ成功......");
        } catch(Exception e) {
            System.err.println("增加秒杀商品时，推送消息到MQ失败：" + e);
            return "failure";
        }

        return "success";
    }

}

package com.tpvlog.seckill.operation.service;

import com.alibaba.fastjson.JSONObject;
import com.tpvlog.seckill.operation.dao.SeckillSessionDAO;
import com.tpvlog.seckill.common.JedisManager;
import com.tpvlog.seckill.operation.domain.SeckillSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

/**
 * 秒杀场次Service组件
 */
@Service
public class SeckillSessionServiceImpl implements SeckillSessionService {

    /**
     * 秒杀场次DAO组件
     */
    @Autowired
    private SeckillSessionDAO seckillSessionDAO;

    /**
     * 增加秒杀场次
     * @param seckillSession 秒杀场次
     */
    public void add(SeckillSession seckillSession) {
        seckillSessionDAO.add(seckillSession);

        JedisManager jedisManager = JedisManager.getInstance();
        Jedis jedis = jedisManager.getJedis();
        jedis.lpush("seckill::sessions::" + seckillSession.getSessionDate(),
                JSONObject.toJSONString(seckillSession));

        // 每一天你都可以有很多的场次，所以真正做的话，在这里，其实是应该在当天的场次list
        // redis的数据结构，list效果
        // 放一天的每一个场次都应该放到里面去
    }

}

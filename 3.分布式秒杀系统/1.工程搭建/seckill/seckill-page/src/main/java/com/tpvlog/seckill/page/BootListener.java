package com.tpvlog.seckill.page;

import com.alibaba.fastjson.JSONObject;
import com.tpvlog.seckill.page.freemarker.FreemarkerHelper;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BootListener implements CommandLineRunner {

    public void run(String... strings) throws Exception {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(
                "seckill-page-consumer-group");
        consumer.setNamesrvAddr("localhost:9876");

        consumer.subscribe("seckill_product_added_topic", "*");

        consumer.registerMessageListener(new MessageListenerConcurrently() {

            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> messageExts,
                                                            ConsumeConcurrentlyContext context) {
                for(MessageExt messageExt : messageExts) {
                    System.out.println(new String(messageExt.getBody()));

                    // 得知，秒杀场次里增加的是哪个商品
                    String seckillProductJSON = new String(messageExt.getBody());
                    JSONObject seckillProductJSONObject = JSONObject.parseObject(seckillProductJSON);

                    // 应该是调用商品中心的接口，获取你需要的商品的全部数据过来
                    Long productId = seckillProductJSONObject.getLong("productId");
                    Double seckillPrice = seckillProductJSONObject.getDouble("seckillPrice");
                    Long seckillStock = seckillProductJSONObject.getLong("seckillStock");

                    // 使用freemarker模板引擎把商品数据渲染到一个html模板里去
                    FreemarkerHelper viewEngine = new FreemarkerHelper();

                    Map<String, Object> paras = new HashMap<String, Object>();
                    paras.put("productId", productId);
                    paras.put("seckillPrice", seckillPrice);
                    paras.put("seckillStock", seckillStock);

                    String html = viewEngine.parseTemplate("autolist.ftl", paras);

                    // 渲染得到的静态html页面文件scp传输到nginx服务器的指定目录下去
                    System.out.println("将渲染完毕的秒杀商品html页面写入磁盘文件......");
                    System.out.println(html);
                    System.out.println("将磁盘上的html文件使用scp命令传送到nginx服务器上去......");

                    // 调用你的云厂商的CDN产品的API，让CDN刷新一下静态页面的缓存
                    System.out.println("调用CDN产品的API，让CDN刷新一下静态页面的缓存.......");
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });

        consumer.start();
        System.out.println("消费者启动......");
    }

}

package com.tpvlog.rpc.protocol.serialization;


public enum SerializationTypeEnum {
    HESSIAN(0x10),
    JSON(0x20);

    private final Integer type;

    SerializationTypeEnum(Integer type) {
        this.type = type;
    }

    public static SerializationTypeEnum findByType(byte serializationType) {
        for (SerializationTypeEnum typeEnum : SerializationTypeEnum.values()) {
            if (typeEnum.getType() == serializationType) {
                return typeEnum;
            }
        }
        return HESSIAN;
    }

    public Integer getType() {
        return type;
    }
}

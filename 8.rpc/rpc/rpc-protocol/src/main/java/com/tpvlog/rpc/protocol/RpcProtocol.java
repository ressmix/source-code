package com.tpvlog.rpc.protocol;


import java.io.Serializable;

/**
 * RPC协议定义
 * @param <T>   协议体类型
 */
public class RpcProtocol<T> implements Serializable {
    /**
     * 协议头
     */
    private MsgHeader header;
    /**
     * 协议体
     */
    private T body;

    public MsgHeader getHeader() {
        return header;
    }

    public void setHeader(MsgHeader header) {
        this.header = header;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}

package com.tpvlog.rpc.protocol;


public enum MsgType {
    REQUEST(1),
    RESPONSE(2),
    HEARTBEAT(3);

    private final Integer type;

    MsgType(Integer type) {
        this.type = type;
    }

    public static MsgType findByType(Integer type) {
        for (MsgType msgType : MsgType.values()) {
            if (msgType.getType() == type) {
                return msgType;
            }
        }
        return null;
    }

    public Integer getType() {
        return type;
    }
}

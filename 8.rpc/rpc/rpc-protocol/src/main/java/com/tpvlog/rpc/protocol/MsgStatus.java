package com.tpvlog.rpc.protocol;


public enum MsgStatus {
    SUCCESS(0),
    FAIL(1);

    private final Integer code;

    MsgStatus(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}

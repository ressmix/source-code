package com.tpvlog.rpc.protocol.handler;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * RPC请求处理器
 */
public class RpcRequestProcessor {

    private static ThreadPoolExecutor threadPoolExecutor;

    static {
        threadPoolExecutor =new ThreadPoolExecutor(10, 10,
                60L, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10000));
    }

    public static void submitRequest(Runnable task) {
        threadPoolExecutor.submit(task);
    }
}

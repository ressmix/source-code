package com.tpvlog.rpc.protocol.codec;

import com.tpvlog.rpc.core.RpcRequest;
import com.tpvlog.rpc.core.RpcResponse;
import com.tpvlog.rpc.protocol.MsgHeader;
import com.tpvlog.rpc.protocol.MsgType;
import com.tpvlog.rpc.protocol.ProtocolConstants;
import com.tpvlog.rpc.protocol.RpcProtocol;
import com.tpvlog.rpc.protocol.serialization.RpcSerialization;
import com.tpvlog.rpc.protocol.serialization.SerializationFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.io.IOException;
import java.util.List;

public class RpcDecoder extends ByteToMessageDecoder {

    /*
    +---------------------------------------------------------------+
    | 魔数 2byte | 协议版本号 1byte | 序列化算法 1byte | 报文类型 1byte  |
    +---------------------------------------------------------------+
    | 状态 1byte |        消息 ID 8byte     |      数据长度 4byte     |
    +---------------------------------------------------------------+
    |                   数据内容 （长度不定）                          |
    +---------------------------------------------------------------+
    */
    @Override
    public final void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws IOException {
        // 保证读到完整协议头
        if (in.readableBytes() < ProtocolConstants.HEADER_TOTAL_LEN) {
            return;
        }
        // mark读指针
        in.markReaderIndex();
        // 魔数
        short magic = in.readShort();
        if (magic != ProtocolConstants.MAGIC) {
            throw new IllegalArgumentException("magic number is illegal, " + magic);
        }
        // 协议版本号
        byte version = in.readByte();
        // 序列号
        byte serializeType = in.readByte();
        // 报文类型
        byte msgType = in.readByte();
        // 状态
        byte status = in.readByte();
        // 请求ID
        long requestId = in.readLong();
        // 协议体长度
        int dataLength = in.readInt();

        // 协议体是否足够读取
        if (in.readableBytes() < dataLength) {
            // 重置读指针
            in.resetReaderIndex();
            return;
        }
        byte[] data = new byte[dataLength];
        in.readBytes(data);

        MsgType msgTypeEnum = MsgType.findByType((int)msgType);
        if (msgTypeEnum == null) {
            return;
        }
        // 封装协议头对象
        MsgHeader header = new MsgHeader();
        header.setMagic(magic);
        header.setVersion(version);
        header.setSerialization(serializeType);
        header.setStatus(status);
        header.setRequestId(requestId);
        header.setMsgType(msgType);
        header.setMsgLen(dataLength);

        RpcSerialization rpcSerialization = SerializationFactory.getRpcSerialization(serializeType);
        switch (msgTypeEnum) {
            case REQUEST:
                RpcRequest request = rpcSerialization.deserialize(data, RpcRequest.class);
                if (request != null) {
                    RpcProtocol<RpcRequest> protocol = new RpcProtocol<>();
                    protocol.setHeader(header);
                    protocol.setBody(request);
                    out.add(protocol);
                }
                break;
            case RESPONSE:
               RpcResponse response = rpcSerialization.deserialize(data, RpcResponse.class);
                if (response != null) {
                    RpcProtocol<RpcResponse> protocol = new RpcProtocol<>();
                    protocol.setHeader(header);
                    protocol.setBody(response);
                    out.add(protocol);
                }
                break;
            case HEARTBEAT:
                // TODO
                break;
        }
    }
}

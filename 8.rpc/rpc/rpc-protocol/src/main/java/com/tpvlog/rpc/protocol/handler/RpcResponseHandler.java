package com.tpvlog.rpc.protocol.handler;

import com.tpvlog.rpc.core.RpcFuture;
import com.tpvlog.rpc.core.RpcResponse;
import com.tpvlog.rpc.core.utils.RpcServiceHelper;
import com.tpvlog.rpc.protocol.RpcProtocol;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * RPC响应处理Handler
 */
public class RpcResponseHandler extends SimpleChannelInboundHandler<RpcProtocol<RpcResponse>> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcProtocol<RpcResponse> msg) {
        long requestId = msg.getHeader().getRequestId();
        // 获取与请求关联的响应对象
        RpcFuture<RpcResponse> future = RpcServiceHelper.REQUEST_MAP.remove(requestId);
        // 设置结果
        future.getPromise().setSuccess(msg.getBody());
    }
}


package com.tpvlog.rpc.registry;

import com.tpvlog.rpc.core.ServiceMeta;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RegistryTest {

    private RegistryService registryService;

    @Before
    public void init() throws Exception {
        registryService = RegistryFactory.getInstance("127.0.0.1:2181", RegistryType.ZOOKEEPER);
    }

    @After
    public void close() throws Exception {
        registryService.destroy();
    }

    @Test
    public void testAll() throws Exception {
        ServiceMeta serviceMeta1 = new ServiceMeta();
        serviceMeta1.setAddress("127.0.0.1");
        serviceMeta1.setPort(8080);
        serviceMeta1.setService("test1");
        serviceMeta1.setVersion("1.0.0");

        ServiceMeta serviceMeta2 = new ServiceMeta();
        serviceMeta2.setAddress("127.0.0.2");
        serviceMeta2.setPort(8080);
        serviceMeta2.setService("test2");
        serviceMeta2.setVersion("1.0.0");

        ServiceMeta serviceMeta3 = new ServiceMeta();
        serviceMeta3.setAddress("127.0.0.3");
        serviceMeta3.setPort(8080);
        serviceMeta3.setService("test3");
        serviceMeta3.setVersion("1.0.0");

        registryService.register(serviceMeta1);
        registryService.register(serviceMeta2);
        registryService.register(serviceMeta3);

        ServiceMeta discovery1 = registryService.discovery("test1#1.0.0", "test1".hashCode());
        ServiceMeta discovery2 = registryService.discovery("test2#1.0.0", "test2".hashCode());
        ServiceMeta discovery3 = registryService.discovery("test3#1.0.0", "test3".hashCode());

        assert discovery1 != null;
        assert discovery2 != null;
        assert discovery3 != null;

        registryService.unRegister(discovery1);
        registryService.unRegister(discovery2);
        registryService.unRegister(discovery3);
    }
}

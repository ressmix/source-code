package com.tpvlog.rpc.registry;

import com.tpvlog.rpc.core.ServiceMeta;

public class EurekaRegistryService implements RegistryService {

    public EurekaRegistryService(String registryAddr) {
    }

    @Override
    public void register(ServiceMeta serviceMeta) {
    }

    @Override
    public void unRegister(ServiceMeta serviceMeta) {
    }

    @Override
    public ServiceMeta discovery(String serviceName, int invokerHashCode) {
        return null;
    }

    @Override
    public void destroy() {
    }
}

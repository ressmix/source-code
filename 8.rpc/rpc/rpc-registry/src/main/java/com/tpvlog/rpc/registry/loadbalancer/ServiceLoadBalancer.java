package com.tpvlog.rpc.registry.loadbalancer;

import java.util.List;

/**
 * 负载均衡器
 * @param <T>
 */
public interface ServiceLoadBalancer<T> {
    T select(List<T> servers, int hashCode);
}
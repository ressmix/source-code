package com.tpvlog.rpc.registry;

import java.util.HashMap;
import java.util.Map;

import static com.tpvlog.rpc.registry.RegistryType.EUREKA;
import static com.tpvlog.rpc.registry.RegistryType.ZOOKEEPER;

public class RegistryFactory {

    private static final Map<RegistryType, RegistryService> registryMap = new HashMap<>();

    public synchronized static RegistryService getInstance(String registryAddr, RegistryType type) throws Exception {
        switch (type) {
            case ZOOKEEPER:
                RegistryService zkReg = registryMap.get(ZOOKEEPER);
                if (zkReg == null) {
                    zkReg = new ZookeeperRegistryService(registryAddr);
                    registryMap.put(ZOOKEEPER, zkReg);
                }
                return zkReg;
            case EUREKA:
                RegistryService eurekaReg = registryMap.get(EUREKA);
                if (eurekaReg == null) {
                    eurekaReg = new EurekaRegistryService(registryAddr);
                    registryMap.put(EUREKA, eurekaReg);
                }
                return eurekaReg;
        }
        throw new UnsupportedOperationException("Unsupported registry type :" + type);
    }
}

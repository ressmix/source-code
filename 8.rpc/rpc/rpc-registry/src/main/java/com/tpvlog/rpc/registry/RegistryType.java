package com.tpvlog.rpc.registry;

public enum RegistryType {
    ZOOKEEPER, EUREKA;
}

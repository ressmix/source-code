package com.tpvlog.rpc.registry;


import com.tpvlog.rpc.core.ServiceMeta;

import java.io.IOException;

/**
 * 服务注册管理
 */
public interface RegistryService {

    void register(ServiceMeta serviceMeta) throws Exception;

    void unRegister(ServiceMeta serviceMeta) throws Exception;

    ServiceMeta discovery(String serviceName, int invokerHashCode) throws Exception;

    void destroy() throws IOException;
}

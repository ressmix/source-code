package com.tpvlog.rpc.facade;

public interface HelloService {
    String hello(String name);
}

package com.tpvlog.rpc.consumer;

import com.tpvlog.rpc.core.RpcFuture;
import com.tpvlog.rpc.core.RpcRequest;
import com.tpvlog.rpc.core.RpcResponse;
import com.tpvlog.rpc.core.utils.RpcServiceHelper;
import com.tpvlog.rpc.protocol.*;
import com.tpvlog.rpc.protocol.serialization.SerializationTypeEnum;
import com.tpvlog.rpc.registry.RegistryService;
import io.netty.channel.DefaultEventLoop;
import io.netty.util.concurrent.DefaultPromise;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class RpcInvokerProxy implements InvocationHandler {

    private final String serviceVersion;
    private final long timeout;
    private final RegistryService registryService;

    public RpcInvokerProxy(String serviceVersion, long timeout, RegistryService registryService) {
        this.serviceVersion = serviceVersion;
        this.timeout = timeout;
        this.registryService = registryService;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long requestId = RpcServiceHelper.REQUEST_ID_GEN.incrementAndGet();

        RpcProtocol<RpcRequest> protocol = new RpcProtocol<>();

        // 协议头
        MsgHeader header = new MsgHeader();
        header.setMagic(ProtocolConstants.MAGIC);
        header.setVersion(ProtocolConstants.VERSION);
        header.setRequestId(requestId);
        header.setSerialization( SerializationTypeEnum.HESSIAN.getType().byteValue());
        header.setMsgType( MsgType.REQUEST.getType().byteValue());
        header.setStatus(MsgStatus.FAIL.getCode().byteValue());
        protocol.setHeader(header);

        // 协议体
        RpcRequest request = new RpcRequest();
        request.setServiceVersion(this.serviceVersion);
        request.setClassName(method.getDeclaringClass().getName());
        request.setMethodName(method.getName());
        request.setParameterTypes(method.getParameterTypes());
        request.setParams(args);
        protocol.setBody(request);

        // 发起请求
        RpcConsumer rpcConsumer = new RpcConsumer();
        RpcFuture<RpcResponse> future = new RpcFuture<>(new DefaultPromise<>(new DefaultEventLoop()), timeout);
        // 关联请求和响应Future对象
        RpcServiceHelper.REQUEST_MAP.put(requestId, future);
        rpcConsumer.sendRequest(protocol, this.registryService);

        return future.getPromise().get(future.getTimeout(), TimeUnit.MILLISECONDS).getData();
    }
}

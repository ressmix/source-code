package com.tpvlog.rpc.consumer.controller;

import com.tpvlog.rpc.consumer.annotation.RpcReference;
import com.tpvlog.rpc.facade.HelloService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RpcReference(version = "1.0.0", timeout = 8000)
    private HelloService helloService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String sayHello() {
        return helloService.hello("mini rpc");
    }
}

package com.tpvlog.rpc.provider;

import com.tpvlog.rpc.core.config.RpcConfigProperties;
import com.tpvlog.rpc.registry.RegistryFactory;
import com.tpvlog.rpc.registry.RegistryService;
import com.tpvlog.rpc.registry.RegistryType;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
@EnableConfigurationProperties(RpcConfigProperties.class)
public class RpcProviderAutoConfiguration {

    @Resource
    private RpcConfigProperties rpcProperties;

    @Bean
    public RpcProviderInitializer init() throws Exception {
        RegistryType type = RegistryType.valueOf(rpcProperties.getRegistryType());
        RegistryService serviceRegistry = RegistryFactory.getInstance(rpcProperties.getRegistryAddr(), type);
        return new RpcProviderInitializer(rpcProperties.getServicePort(), serviceRegistry);
    }
}

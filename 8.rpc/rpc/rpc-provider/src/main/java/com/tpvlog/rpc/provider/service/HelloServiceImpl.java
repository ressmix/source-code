package com.tpvlog.rpc.provider.service;

import com.tpvlog.rpc.facade.HelloService;
import com.tpvlog.rpc.provider.annotation.RpcService;

@RpcService(service = HelloService.class, version = "1.0.0")
public class HelloServiceImpl implements HelloService {
    @Override
    public String hello(String name) {
        return "hello" + name;
    }
}

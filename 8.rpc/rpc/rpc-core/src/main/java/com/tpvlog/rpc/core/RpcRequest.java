package com.tpvlog.rpc.core;

import java.io.Serializable;
import java.util.Arrays;

public class RpcRequest implements Serializable {
    /**
     * 服务版本号
     */
    private String serviceVersion;
    /**
     * 服务类全限定名
     */
    private String className;
    /**
     * 服务接口全限定名
     */
    private String methodName;
    /**
     * 接口参数值
     */
    private Object[] params;
    /**
     * 接口参数类型
     */
    private Class<?>[] parameterTypes;

    public String getServiceVersion() {
        return serviceVersion;
    }

    public void setServiceVersion(String serviceVersion) {
        this.serviceVersion = serviceVersion;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(Class<?>[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    @Override
    public String toString() {
        return "RpcRequest{" +
                "serviceVersion='" + serviceVersion + '\'' +
                ", className='" + className + '\'' +
                ", methodName='" + methodName + '\'' +
                ", params=" + Arrays.toString(params) +
                ", parameterTypes=" + Arrays.toString(parameterTypes) +
                '}';
    }
}

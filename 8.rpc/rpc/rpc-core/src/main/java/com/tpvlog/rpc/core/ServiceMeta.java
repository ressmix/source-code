package com.tpvlog.rpc.core;


/**
 * 服务元数据
 */
public class ServiceMeta {

    /**
     * 服务类全限定名
     */
    private String service;

    /**
     * 服务版本号
     */
    private String version;

    /**
     * 服务实例地址
     */
    private String address;

    /**
     * 服务端实例端口
     */
    private Integer port;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}

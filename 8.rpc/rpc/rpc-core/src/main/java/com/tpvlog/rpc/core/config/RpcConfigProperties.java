package com.tpvlog.rpc.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "rpc")
public class RpcConfigProperties {

    /**
     * RPC服务端口
     */
    private int servicePort;

    /**
     * 服务注册中心地址
     */
    private String registryAddr;

    /**
     * 服务注册中心类型
     */
    private String registryType;

    public int getServicePort() {
        return servicePort;
    }

    public void setServicePort(int servicePort) {
        this.servicePort = servicePort;
    }

    public String getRegistryAddr() {
        return registryAddr;
    }

    public void setRegistryAddr(String registryAddr) {
        this.registryAddr = registryAddr;
    }

    public String getRegistryType() {
        return registryType;
    }

    public void setRegistryType(String registryType) {
        this.registryType = registryType;
    }
}

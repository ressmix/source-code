package com.tpvlog.rpc.core.utils;

import com.tpvlog.rpc.core.RpcFuture;
import com.tpvlog.rpc.core.RpcResponse;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * RPC服务工具类
 */
public class RpcServiceHelper {
    /**
     * 请求ID生成器
     */
    public static final AtomicLong REQUEST_ID_GEN = new AtomicLong(0);

    /**
     * 请求/响应映射缓存
     */
    public static final Map<Long, RpcFuture<RpcResponse>> REQUEST_MAP = new ConcurrentHashMap<>();

    /**
     * 根据服务名和版本号生成Key
     *
     * @param serviceName    服务名称
     * @param serviceVersion 服务版本号
     * @return
     */
    public static String buildServiceKey(String serviceName, String serviceVersion) {
        return String.join("#", serviceName, serviceVersion);
    }
}

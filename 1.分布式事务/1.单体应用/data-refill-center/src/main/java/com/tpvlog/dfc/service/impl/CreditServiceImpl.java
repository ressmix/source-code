package com.tpvlog.dfc.service.impl;

import com.tpvlog.dfc.mapper.CreditMapper;
import com.tpvlog.dfc.service.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 积分service组件
 */
@Service
public class CreditServiceImpl implements CreditService {

    /**
     * 积分mapper组件
     */
    @Autowired
    private CreditMapper creditMapper;

    /**
     * 增加积分
     *
     * @param userAccountId 用户账号id
     */
    public void increment(Long userAccountId, Double updatedPoint) {
        creditMapper.increment(userAccountId, updatedPoint);
    }

}

package com.tpvlog.dfc.service;

/**
 * 消息服务service组件
 */
public interface MessageService {

    /**
     * 发送短信
     *
     * @param phoneNumber 手机号码
     * @param message     短信消息
     */
    void send(String phoneNumber, String message);

}

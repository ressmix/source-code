package com.tpvlog.dfc.service.impl;

import com.tpvlog.dfc.domain.CouponActivity;
import com.tpvlog.dfc.mapper.CouponActivityMapper;
import com.tpvlog.dfc.service.CouponActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 流量券活动service组件
 */
@Service
public class CouponActivityServiceImpl implements CouponActivityService {

    /**
     * 流量券活动mapper组件
     */
    @Autowired
    private CouponActivityMapper couponActivityMapper;

    /**
     * 查询流量套餐绑定的状态处于"进行中"的流量券活动
     *
     * @return 流量券活动
     */
    public CouponActivity queryByDataPackageId(Long dataPackageId) {
        return couponActivityMapper.queryByDataPackageId(dataPackageId);
    }

}

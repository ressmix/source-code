package com.tpvlog.dfc.service;

import com.tpvlog.dfc.domain.DataPackage;

import java.util.List;

/**
 * 流量套餐service组件
 */
public interface DataPackageService {

    /**
     * 查询所有的流量套餐
     *
     * @return 流量套餐
     */
    List<DataPackage> queryAll();

}

package com.tpvlog.dfc.activity.api.api;

import java.util.List;

import com.tpvlog.dfc.activity.api.mapper.DataPackageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.tpvlog.dfc.activity.api.domain.DataPackage;

/**
 * 流量套餐service组件

 *
 */
@RestController
public class DataPackageService implements DataPackageApi {

	/**
	 * 流量套餐mapper组件
	 */
	@Autowired
	private DataPackageMapper dataPackageMapper;
	
	/**
	 * 查询所有的流量套餐
	 * @return 流量套餐
	 */
	public List<DataPackage> queryAll() {
		return dataPackageMapper.queryAll();
	}
	
}

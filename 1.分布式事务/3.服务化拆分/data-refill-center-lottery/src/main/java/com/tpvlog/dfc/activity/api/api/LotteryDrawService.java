package com.tpvlog.dfc.activity.api.api;

import com.tpvlog.dfc.activity.api.mapper.LotteryDrawMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 抽奖机会service组件

 *
 */
@RestController
public class LotteryDrawService implements LotteryDrawApi {

	/**
	 * 抽奖机会mapper组件
	 */
	@Autowired
	private LotteryDrawMapper lotteryDrawMapper;
	
	/**
	 * 增加一次抽奖次数
	 * @param userAccountId 用户账号id
	 */
	public void increment(@PathVariable("userAccountId") Long userAccountId) {
		lotteryDrawMapper.increment(userAccountId);  
	}
	
}

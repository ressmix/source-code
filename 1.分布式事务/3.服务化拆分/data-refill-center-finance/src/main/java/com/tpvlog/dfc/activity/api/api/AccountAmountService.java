package com.tpvlog.dfc.activity.api.api;

import com.tpvlog.dfc.activity.api.mapper.AccountAmountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 账号金额service组件

 *
 */
@RestController
public class AccountAmountService implements AccountAmountApi {

	/**
	 * 账号金额mapper组件
	 */
	@Autowired
	private AccountAmountMapper accountAmountMapper;
	
	/**
	 * 转账
	 * @param fromUserAccountId 从谁那儿转账
	 * @param toUserAccountId 转到谁那儿去
	 * @param amount 转账金额
	 */
	public void transfer(@RequestParam("fromUserAccountId") Long fromUserAccountId, 
			@RequestParam("toUserAccountId") Long toUserAccountId, 
			@RequestParam("amount") Double amount) {
		accountAmountMapper.updateAmount(fromUserAccountId, -amount);
		accountAmountMapper.updateAmount(toUserAccountId, amount);
	}
	
}

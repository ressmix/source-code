package com.tpvlog.dfc.activity.api.api;

import com.tpvlog.dfc.activity.api.mapper.CouponMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tpvlog.dfc.activity.api.domain.Coupon;

/**
 * 流量券service组件

 *
 */
@RestController
public class CouponService implements CouponApi {
	
	/**
	 * 流量券mapper组件
	 */
	@Autowired
	private CouponMapper couponMapper;
	
	/**
	 * 查询用户账号的面额最高的一张流量券
	 * @param userAccountId 用户账号id
	 * @return 流量券
	 */
	public Coupon queryByUserAccountId(@PathVariable("userAccountId") Long userAccountId) {
		return couponMapper.queryByUserAccountId(userAccountId);
	}
	
	/**
	 * 将流量券标记为已使用
	 * @param id 流量券id
	 */
	public void markCouponUsed(@PathVariable("id") Long id) {
		couponMapper.updateStatus(id, 2);
	}
	
	/**
	 * 插入一张流量券
	 * @param coupon 流量券
	 */
	public void insert(@RequestBody Coupon coupon) {
		couponMapper.insert(coupon); 
	}

}

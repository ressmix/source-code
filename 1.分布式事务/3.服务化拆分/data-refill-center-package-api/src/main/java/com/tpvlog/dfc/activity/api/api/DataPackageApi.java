package com.tpvlog.dfc.activity.api.api;

import java.util.List;

import com.tpvlog.dfc.activity.api.domain.DataPackage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 流量套餐service组件

 *
 */
@RequestMapping("/package")  
public interface DataPackageApi {

	/**
	 * 查询所有的流量套餐
	 * @return 流量套餐
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET) 
	List<DataPackage> queryAll();
	
}

package com.tpvlog.dfc.activity.api.api;

import java.util.List;

import com.tpvlog.dfc.activity.api.mapper.RefillOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tpvlog.dfc.activity.api.domain.RefillOrder;

/**
 * 充值订单service组件

 *
 */
@RestController
public class RefillOrderService implements RefillOrderApi {

	/**
	 * 充值订单mapper组件
	 */
	@Autowired
	private RefillOrderMapper refillOrderMapper;
	
	/**
	 * 增加一个充值订单
	 * @param refillOrder 充值订单
	 */
	public void add(@RequestBody RefillOrder refillOrder) {
		refillOrderMapper.add(refillOrder); 
	}
	
	/**
	 * 查询所有的充值订单
	 * @return
	 */
	public List<RefillOrder> queryAll(@PathVariable("userAccountId") Long userAccountId) {
		return refillOrderMapper.queryAll(userAccountId) ;
	}
	
	/**
	 * 查询充值订单
	 * @param id 充值订单id
	 * @return
	 */
	public RefillOrder queryById(@PathVariable("id") Long id) {
		return refillOrderMapper.queryById(id);
	}
	
}

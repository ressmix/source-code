package com.tpvlog.dfc.activity.api.api;

import com.tpvlog.dfc.activity.api.mapper.CreditMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 积分service组件

 *
 */
@RestController
public class CreditService implements CreditApi {

	/**
	 * 积分mapper组件
	 */
	@Autowired
	private CreditMapper creditMapper;
	
	/**
	 * 增加积分
	 * @param userAccountId 用户账号id
	 */
	public void increment(@PathVariable("userAccountId")Long userAccountId, 
			@RequestParam("updatedPoint") Double updatedPoint) {
		creditMapper.increment(userAccountId, updatedPoint); 
	}
	
}

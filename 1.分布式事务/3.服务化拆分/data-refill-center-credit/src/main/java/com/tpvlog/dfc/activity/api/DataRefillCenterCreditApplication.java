package com.tpvlog.dfc.activity.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Import;

import com.tpvlog.dfc.activity.api.db.DataSourceConfig;

/**
 * 活动服务

 *
 */
@SpringBootApplication
@ServletComponentScan
@EnableEurekaClient
@EnableFeignClients
@Import(DataSourceConfig.class)
public class DataRefillCenterCreditApplication {
	
	public static void main(String[] args) { 
		SpringApplication.run(DataRefillCenterCreditApplication.class, args);
	}

}

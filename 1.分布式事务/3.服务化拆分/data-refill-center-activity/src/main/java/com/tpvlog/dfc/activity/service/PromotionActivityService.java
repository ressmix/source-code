package com.tpvlog.dfc.activity.service;

import com.tpvlog.dfc.activity.api.PromotionActivityApi;
import com.tpvlog.dfc.activity.domain.PromotionActivity;
import com.tpvlog.dfc.activity.mapper.PromotionActivityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 优惠活动service组件
 */
@RestController
public class PromotionActivityService implements PromotionActivityApi {

    /**
     * 优惠活动mapper组件
     */
    @Autowired
    private PromotionActivityMapper promotionActivityMapper;

    /**
     * 查询流量套餐绑定的状态处于"进行中"的优惠活动
     *
     * @return 优惠活动
     */
    public PromotionActivity queryByDataPackageId(
            @PathVariable("dataPackageId") Long dataPackageId) {
        return promotionActivityMapper.queryByDataPackageId(dataPackageId);
    }

}

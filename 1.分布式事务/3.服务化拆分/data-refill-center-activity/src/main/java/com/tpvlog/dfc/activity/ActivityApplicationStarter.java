package com.tpvlog.dfc.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 促销服务
 */
@SpringBootApplication(scanBasePackages = {"com.tpvlog.dfc.activity"})
@EnableEurekaClient
@EnableFeignClients
public class ActivityApplicationStarter {
    public static void main(String[] args) {
        SpringApplication.run(ActivityApplicationStarter.class, args);
    }
}

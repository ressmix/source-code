package com.tpvlog.dfc;

import com.mysql.jdbc.jdbc2.optional.MysqlXAConnection;
import com.mysql.jdbc.jdbc2.optional.MysqlXid;

import javax.sql.XAConnection;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQLXATest {

    public static void main(String[] args) throws SQLException {
        // 1.创建抽奖库的RM实例
        Connection lotteryConnection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/data-refill-center-lottery",
                "root",
                "root");
        XAConnection lotteryXAConnection = new MysqlXAConnection(
                (com.mysql.jdbc.Connection) lotteryConnection, true);
        // XAResource可以认为是一个RM（Resource Manager）实例
        XAResource lotteryResource = lotteryXAConnection.getXAResource();

        // 2.创建积分库的RM实例
        Connection creditConnection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/data-refill-center-credit",
                "root",
                "root");
        XAConnection creditXAConnection = new MysqlXAConnection(
                (com.mysql.jdbc.Connection) creditConnection, true);
        XAResource creditResource = creditXAConnection.getXAResource();

        // 分布式事务id（txid）
        byte[] gtrid = "g12345".getBytes();
        int formatId = 1;

        try {
            // 抽奖库的子事务标识
            byte[] bqual1 = "b00001".getBytes();
            Xid xid1 = new MysqlXid(gtrid, bqual1, formatId);

            // 3.定义抽奖库的子事务操作，注意这里只是先定义，实际还不会执行
            lotteryResource.start(xid1, XAResource.TMNOFLAGS);
            PreparedStatement lotteryPreparedStatement = lotteryConnection.prepareStatement(
                    "UPDATE lottery_draw SET lottery_draw_count=lottery_draw_count+1 WHERE id=1");
            lotteryPreparedStatement.execute();
            lotteryResource.end(xid1, XAResource.TMSUCCESS);

            // 积分库的子事务标识
            byte[] bqual2 = "b00002".getBytes();
            Xid xid2 = new MysqlXid(gtrid, bqual2, formatId);

            // 4.定义积分库的子事务操作，注意这里只是先定义，实际还不会执行
            creditResource.start(xid2, XAResource.TMNOFLAGS);
            PreparedStatement creditPreparedStatement = creditConnection.prepareStatement(
                    "UPDATE credit SET POINT=POINT+1.2 WHERE id=1");
            creditPreparedStatement.execute();
            creditResource.end(xid2, XAResource.TMSUCCESS);

            // 到这里为止，只是定义了分布式事务中的两个库要执行的SQL操作，实际还啥都执行

            // 5.2PC的阶段一：向两个库都发送prepare消息，执行事务中的SQL语句，但是不提交
            int lotteryPrepareResult = lotteryResource.prepare(xid1);
            int creditPrepareResult = creditResource.prepare(xid2);

            // 6.2PC的阶段二：两个库都发送commit消息，提交事务
            if (lotteryPrepareResult == XAResource.XA_OK
                    && creditPrepareResult == XAResource.XA_OK) {
                // 如果两个库对prepare都返回ok，那么就全部commit
                lotteryResource.commit(xid1, false);
                creditResource.commit(xid2, false);
            }else {
                // 如果如果不是所有库都对prepare返回ok，那么就全部rollback
                lotteryResource.rollback(xid1);
                creditResource.rollback(xid2);
            }
        } catch (XAException e) {
            e.printStackTrace();
        }
    }
}
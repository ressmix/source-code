package com.tpvlog.dfc.db;

import com.alibaba.druid.pool.xa.DruidXADataSource;
import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.icatch.jta.UserTransactionManager;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.sql.DataSource;
import javax.transaction.UserTransaction;

/**
 * druid数据库连接池配置类
 */
@Configuration
@MapperScan(basePackages = "com.tpvlog.dfc.mapper.activity",
        sqlSessionFactoryRef = "activitySqlSessionFactory")
public class ActivityDataSourceConfig {

    @Value("${activity.datasource.url}")
    private String dbUrl;
    @Value("${activity.datasource.username}")
    private String username;
    @Value("${activity.datasource.password}")
    private String password;
    @Value("${activity.datasource.driverClassName}")
    private String driverClassName;

    /**
     * 创建druid数据库连接池bean
     *
     * @return
     */
    @Bean(name = "activityDataSource")
    @Primary
    public DataSource activityDataSource() {
        DruidXADataSource datasource = new DruidXADataSource();
        datasource.setUrl(this.dbUrl);
        datasource.setUsername(username);
        datasource.setPassword(password);
        datasource.setDriverClassName(driverClassName);

        AtomikosDataSourceBean atomikosDataSource = new AtomikosDataSourceBean();
        atomikosDataSource.setXaDataSource(datasource);

        return atomikosDataSource;
    }

    @Bean(name = "xatx")
    @Primary
    public JtaTransactionManager activityTransactionManager() {
        UserTransactionManager userTransactionManager = new UserTransactionManager();
        UserTransaction userTransaction = new UserTransactionImp();
        return new JtaTransactionManager(userTransaction, userTransactionManager);
    }

    @Bean(name = "activitySqlSessionFactory")
    @Primary
    public SqlSessionFactory activitySqlSessionFactory(
            @Qualifier("activityDataSource") DataSource activityDataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(activityDataSource);
        return sessionFactory.getObject();
    }
}
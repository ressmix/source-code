package com.tpvlog.dfc.db;

import com.alibaba.druid.pool.xa.DruidXADataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * druid数据库连接池配置类
 */
@Configuration
@MapperScan(basePackages = "com.tpvlog.dfc.mapper.coupon",
        sqlSessionFactoryRef = "couponSqlSessionFactory")
public class CouponDataSourceConfig {

    @Value("${coupon.datasource.url}")
    private String dbUrl;
    @Value("${coupon.datasource.username}")
    private String username;
    @Value("${coupon.datasource.password}")
    private String password;
    @Value("${coupon.datasource.driverClassName}")
    private String driverClassName;

    /**
     * 创建druid数据库连接池bean
     *
     * @return
     */
    @Bean(name = "couponDataSource")
    public DataSource couponDataSource() {
        DruidXADataSource datasource = new DruidXADataSource();
        datasource.setUrl(this.dbUrl);
        datasource.setUsername(username);
        datasource.setPassword(password);
        datasource.setDriverClassName(driverClassName);

        AtomikosDataSourceBean atomikosDataSource = new AtomikosDataSourceBean();
        atomikosDataSource.setXaDataSource(datasource);

        return atomikosDataSource;
    }

    @Bean(name = "couponSqlSessionFactory")
    public SqlSessionFactory couponSqlSessionFactory(
            @Qualifier("couponDataSource") DataSource couponDataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(couponDataSource);
        return sessionFactory.getObject();
    }

}
package com.tpvlog.dfc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 流量充值中心启动类
 */
@SpringBootApplication(scanBasePackages = {"com.tpvlog.dfc"})
public class ApplicationStarter {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationStarter.class, args);
    }
}

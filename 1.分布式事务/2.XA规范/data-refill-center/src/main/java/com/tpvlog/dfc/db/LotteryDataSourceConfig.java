package com.tpvlog.dfc.db;

import com.alibaba.druid.pool.xa.DruidXADataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * druid数据库连接池配置类
 */
@Configuration
@MapperScan(basePackages = "com.tpvlog.dfc.mapper.lottery",
        sqlSessionFactoryRef = "lotterySqlSessionFactory")
public class LotteryDataSourceConfig {

    @Value("${lottery.datasource.url}")
    private String dbUrl;
    @Value("${lottery.datasource.username}")
    private String username;
    @Value("${lottery.datasource.password}")
    private String password;
    @Value("${lottery.datasource.driverClassName}")
    private String driverClassName;

    /**
     * 创建druid数据库连接池bean
     *
     * @return
     */
    @Bean(name = "lotteryDataSource")
    public DataSource lotteryDataSource() {
        DruidXADataSource datasource = new DruidXADataSource();
        datasource.setUrl(this.dbUrl);
        datasource.setUsername(username);
        datasource.setPassword(password);
        datasource.setDriverClassName(driverClassName);

        AtomikosDataSourceBean atomikosDataSource = new AtomikosDataSourceBean();
        atomikosDataSource.setXaDataSource(datasource);

        return atomikosDataSource;
    }

    @Bean(name = "lotterySqlSessionFactory")
    public SqlSessionFactory lotterySqlSessionFactory(
            @Qualifier("lotteryDataSource") DataSource lotteryDataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(lotteryDataSource);
        return sessionFactory.getObject();
    }
}
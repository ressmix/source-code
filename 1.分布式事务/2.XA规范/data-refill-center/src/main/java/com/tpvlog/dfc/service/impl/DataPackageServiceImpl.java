package com.tpvlog.dfc.service.impl;

import com.tpvlog.dfc.domain.DataPackage;
import com.tpvlog.dfc.mapper.datapackage.DataPackageMapper;
import com.tpvlog.dfc.service.DataPackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 流量套餐service组件
 */
@Service
public class DataPackageServiceImpl implements DataPackageService {

    /**
     * 流量套餐mapper组件
     */
    @Autowired
    private DataPackageMapper dataPackageMapper;

    /**
     * 查询所有的流量套餐
     *
     * @return 流量套餐
     */
    public List<DataPackage> queryAll() {
        return dataPackageMapper.queryAll();
    }

}

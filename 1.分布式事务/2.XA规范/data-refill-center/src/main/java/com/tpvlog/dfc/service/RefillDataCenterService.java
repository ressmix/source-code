package com.tpvlog.dfc.service;

import com.tpvlog.dfc.domain.RefillRequest;

/**
 * 流量充值中心service组件
 */
public interface RefillDataCenterService {

    /**
     * 完成流量充值
     *
     * @param refillRequest
     */
    void finishRefillData(RefillRequest refillRequest);

}

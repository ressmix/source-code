package com.tpvlog.dfc.mapper.credit;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 积分mapper组件
 */
@Mapper
public interface CreditMapper {

    /**
     * 增加积分
     *
     * @param userAccountId 用户账号id
     */
    @Update("UPDATE credit "
//			+ "SET point = point + #{updatedPoint} "
            + "SET point='aaa' "
            + "WHERE user_account_id=#{userAccountId}")
    void increment(@Param("userAccountId") Long userAccountId,
                   @Param("updatedPoint") Double updatedPoint);

}

package com.tpvlog.dfc.service.impl;

import com.tpvlog.dfc.mapper.lottery.LotteryDrawMapper;
import com.tpvlog.dfc.service.LotteryDrawService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 抽奖机会service组件
 */
@Service
public class LotteryDrawServiceImpl implements LotteryDrawService {

    /**
     * 抽奖机会mapper组件
     */
    @Autowired
    private LotteryDrawMapper lotteryDrawMapper;

    /**
     * 增加一次抽奖次数
     *
     * @param userAccountId 用户账号id
     */
    public void increment(Long userAccountId) {
        lotteryDrawMapper.increment(userAccountId);
    }

}

package com.tpvlog.dfc.service.impl;

import com.tpvlog.dfc.domain.RefillOrder;
import com.tpvlog.dfc.mapper.order.RefillOrderMapper;
import com.tpvlog.dfc.service.RefillOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 充值订单service组件
 */
@Service
public class RefillOrderServiceImpl implements RefillOrderService {

    /**
     * 充值订单mapper组件
     */
    @Autowired
    private RefillOrderMapper refillOrderMapper;

    /**
     * 增加一个充值订单
     *
     * @param refillOrder 充值订单
     */
    public void add(RefillOrder refillOrder) {
        refillOrderMapper.add(refillOrder);
    }

    /**
     * 查询所有的充值订单
     *
     * @return
     */
    public List<RefillOrder> queryAll(Long userAccountId) {
        return refillOrderMapper.queryAll(userAccountId);
    }

    /**
     * 查询充值订单
     *
     * @param id 充值订单id
     * @return
     */
    public RefillOrder queryById(Long id) {
        return refillOrderMapper.queryById(id);
    }

}

package com.tpvlog.drc.server.node.master;

/**
 * 服务治理平台的master节点
 */
public class MasterNode {

    /**
     * Controller候选人
     */
    private ControllerCandidate controllerCandidate;
    /**
     * 集群内部的master节点之间进行网络通信的组件
     */
    private MasterNetworkManager masterNetworkManager;

    /**
     * master节点的构造函数
     */
    public MasterNode() {
        this.masterNetworkManager = new MasterNetworkManager();
        this.controllerCandidate = new ControllerCandidate(masterNetworkManager);
    }

    /**
     * 启动master节点
     */
    public void start() {
        // 初始化所有Master节点互相之间的网络连接
        masterNetworkManager.waitAfterMasterNodesConnect();
        masterNetworkManager.connectBeforeMasterNodes();
    }

}

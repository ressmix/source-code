package com.tpvlog.drc.server.constant;

/**
 * 节点类型
 */
public interface NodeType {

    String MASTER = "master";
    String SLAVE = "slave";

}

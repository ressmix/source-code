package com.tpvlog.drc.server.node;

/**
 * 节点状态
 */
public class NodeStatus {

    /**
     * 正在初始化
     */
    public static final int INITIALIZING = 0;
    /**
     * 正在运行中
     */
    public static final int RUNNING = 1;
    /**
     * 已经关闭了
     */
    public static final int SHUTDOWN = 2;
    /**
     * 节点状态
     */
    private int status;

    private NodeStatus() {

    }

    public static NodeStatus getInstance() {
        return Singleton.instance;
    }

    /**
     * 获取节点状态
     *
     * @return 节点状态
     */
    public static int get() {
        return getInstance().getStatus();
    }

    /**
     * 获取节点状态
     *
     * @return 节点状态
     */
    public synchronized int getStatus() {
        return status;
    }

    /**
     * 设置节点状态
     *
     * @param status 节点状态
     */
    public synchronized void setStatus(int status) {
        this.status = status;
    }

    private static class Singleton {
        static NodeStatus instance = new NodeStatus();
    }
}

package com.tpvlog.drc.server.node.master;

/**
 * Controller候选人
 */
public class ControllerCandidate {

    /**
     * 集群内部的master节点之间进行网络通信的组件
     */
    private MasterNetworkManager mastereNetworkManager;

    /**
     * Controller候选人的构造函数
     *
     * @param mastereNetworkManager
     */
    public ControllerCandidate(MasterNetworkManager mastereNetworkManager) {
        this.mastereNetworkManager = mastereNetworkManager;
    }

}

package com.tpvlog.drc.server.node.master;

import java.net.Socket;

/**
 * master节点间的网络连接的读IO线程
 */
public class MasterNetworkReadThread extends Thread {

    /**
     * master节点间的网络连接
     */
    private Socket socket;

    /**
     * 构造函数
     *
     * @param socket
     */
    public MasterNetworkReadThread(Socket socket) {
        this.socket = socket;
    }

    /**
     * 线程运行逻辑
     */
    @Override
    public void run() {

    }

}

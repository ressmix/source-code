package com.tpvlog.dfs.rpc.service;// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: NameNodeServiceProto.proto

public final class NameNodeServiceProto {
  private NameNodeServiceProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_RegisterRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_RegisterRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_RegisterResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_RegisterResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_HeartbeatRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_HeartbeatRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_HeartbeatResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_HeartbeatResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_MkDirRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_MkDirRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_MkDirResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_MkDirResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\032NameNodeServiceProto.proto\"/\n\017Register" +
      "Request\022\n\n\002ip\030\001 \001(\t\022\020\n\010hostname\030\002 \001(\t\"\"\n" +
      "\020RegisterResponse\022\016\n\006status\030\001 \001(\005\"0\n\020Hea" +
      "rtbeatRequest\022\n\n\002ip\030\001 \001(\t\022\020\n\010hostname\030\002 " +
      "\001(\t\"#\n\021HeartbeatResponse\022\016\n\006status\030\001 \001(\005" +
      "\"\034\n\014MkDirRequest\022\014\n\004path\030\001 \001(\t\"\037\n\rMkDirR" +
      "esponse\022\016\n\006status\030\001 \001(\0052\244\001\n\017NameNodeServ" +
      "ice\0221\n\010register\022\020.RegisterRequest\032\021.Regi" +
      "sterResponse\"\000\0224\n\theartbeat\022\021.HeartbeatR" +
      "equest\032\022.HeartbeatResponse\"\000\022(\n\005mkdir\022\r." +
      "MkDirRequest\032\016.MkDirResponse\"\000B\030B\024NameNo" +
      "deServiceProtoP\001b\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_RegisterRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_RegisterRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_RegisterRequest_descriptor,
        new String[] { "Ip", "Hostname", });
    internal_static_RegisterResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_RegisterResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_RegisterResponse_descriptor,
        new String[] { "Status", });
    internal_static_HeartbeatRequest_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_HeartbeatRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_HeartbeatRequest_descriptor,
        new String[] { "Ip", "Hostname", });
    internal_static_HeartbeatResponse_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_HeartbeatResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_HeartbeatResponse_descriptor,
        new String[] { "Status", });
    internal_static_MkDirRequest_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_MkDirRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_MkDirRequest_descriptor,
        new String[] { "Path", });
    internal_static_MkDirResponse_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_MkDirResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_MkDirResponse_descriptor,
        new String[] { "Status", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}

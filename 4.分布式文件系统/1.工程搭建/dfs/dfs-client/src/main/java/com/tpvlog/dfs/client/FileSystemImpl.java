package com.tpvlog.dfs.client;

import com.tpvlog.dfs.rpc.service.MkDirRequest;
import com.tpvlog.dfs.rpc.service.MkDirResponse;
import com.tpvlog.dfs.rpc.service.NameNodeServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.NegotiationType;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;

/**
 * 文件系统客户端的实现类
 *
 * @author Ressmix
 */
public class FileSystemImpl implements FileSystem {

    // 这里指定NameNode的地址
    private static final String NAMENODE_HOSTNAME = "localhost";
    private static final Integer NAMENODE_PORT = 50070;

    private NameNodeServiceGrpc.NameNodeServiceBlockingStub namenode;

    public FileSystemImpl() {
        ManagedChannel channel = NettyChannelBuilder
                .forAddress(NAMENODE_HOSTNAME, NAMENODE_PORT)
                .negotiationType(NegotiationType.PLAINTEXT)
                .build();
        this.namenode = NameNodeServiceGrpc.newBlockingStub(channel);
    }

    /**
     * 创建目录
     */
    public void mkdir(String path) throws Exception {
        // 调用RPC服务
        MkDirRequest request = MkDirRequest.newBuilder().setPath(path).build();
        MkDirResponse response = namenode.mkdir(request);

        System.out.println("创建目录的响应：" + response.getStatus());
    }
}

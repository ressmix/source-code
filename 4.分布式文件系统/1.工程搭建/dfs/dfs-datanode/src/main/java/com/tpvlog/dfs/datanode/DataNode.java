package com.tpvlog.dfs.datanode;

import com.tpvlog.dfs.datanode.server.NameNodeConnService;

/**
 * DataNode启动类
 * @author Ressmix
 *
 */
public class DataNode {

	private volatile Boolean isRunning;
	/**
	 * 负责跟NameNode通信的组件
	 */
	private NameNodeConnService connService;
	
	/**
	 * 初始化DataNode
	 */
	private void initialize() {
		this.isRunning = true;
		this.connService = new NameNodeConnService();
	}
	
	/**
	 * 运行DataNode
	 */
	private void start() {
		this.connService.start();
		try {
			while(isRunning) {
				Thread.sleep(1000);  
			}   
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		DataNode datanode = new DataNode();
		datanode.initialize();
		datanode.start();
	}
}

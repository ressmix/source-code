package com.tpvlog.dfs.datanode.server;

/**
 * 负责跟NameNode进行通信的组件
 *
 * @author Ressmix
 */
public class NameNodeConnService {

    /**
     * 负责跟NameNode主节点通信的ServiceActor组件
     */
    private NameNodeConnActor serviceActor;

    /**
     * 构造函数
     */
    public NameNodeConnService() {
        this.serviceActor = new NameNodeConnActor();
    }

    public void start() {
        // 节点注册
        register();
        // 节点心跳
        heartbeat();
    }

    /**
     * 向主备两个NameNode节点进行注册
     */
    private void register() {
        this.serviceActor.startRegister();
    }

    /**
     * 开始发送心跳给NameNode
     */
    private void heartbeat() {
        this.serviceActor.startHeartbeat();
    }
}

package com.tpvlog.im.protocal;

import com.google.protobuf.InvalidProtocolBufferException;
import com.tpvlog.im.common.Constants;
import com.tpvlog.im.common.Request;
import io.netty.buffer.ByteBuf;

public class AuthenticateRequestProtoTest {

    public static void main(String[] args) throws InvalidProtocolBufferException {
        // 1.创建一个消息体对象
        AuthenticateRequestProto.AuthenticateRequest.Builder builder =
                AuthenticateRequestProto.AuthenticateRequest.newBuilder();
        builder.setUid("ressmix");
        builder.setToken("==fg1husHWdsdsU2H82NXHdsdx=");
        builder.setTimestamp(System.currentTimeMillis());
        AuthenticateRequestProto.AuthenticateRequest authenticateRequest = builder.build();

        // 2.序列化
        byte[] serializedRequest = authenticateRequest.toByteArray();
        System.out.println("bodyLength:" + serializedRequest.length);

        // 3.封装一个完整的请求消息
        Request request = new Request(Constants.APP_SDK_VERSION_1, Constants.REQUEST_TYPE_AUTHENTICATE,
                Constants.SEQUENCE_DEFAULT, serializedRequest);

        // 4.解析请求，验证结果是否正确
        ByteBuf buffer = request.getBuffer();
        int headerLength = buffer.readInt();
        System.out.println("headerLength:" + headerLength);
        int appSdkVersion = buffer.readInt();
        System.out.println("appSdkVersion:" + appSdkVersion);
        int messageType = buffer.readInt();
        System.out.println("messageType:" + messageType);
        int requestType = buffer.readInt();
        System.out.println("requestType:" + requestType);
        int sequence = buffer.readInt();
        System.out.println("sequence:" + sequence);
        int bodyLength = buffer.readInt();
        System.out.println("bodyLength:" + bodyLength);

        byte[] body = new byte[bodyLength];
        buffer.readBytes(body, 0, bodyLength);

        // 5.反序列化
        AuthenticateRequestProto.AuthenticateRequest deserializedRequest =
                AuthenticateRequestProto.AuthenticateRequest.parseFrom(body);
        System.out.println(deserializedRequest);
    }
}

package com.tpvlog.im.dispatcher;

import io.netty.channel.socket.SocketChannel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GatewayInstanceManager {

    private Map<String, SocketChannel> address2Channel = new ConcurrentHashMap<>();

    private GatewayInstanceManager() {
    }

    public static GatewayInstanceManager getInstance() {
        return GatewayInstanceManagerHolder.INSTANCE;
    }

    public void addGatewayInstance(String channelId, SocketChannel channel) {
        address2Channel.put(channelId, channel);
    }

    public void removeGatewayInstance(String channelId) {
        address2Channel.remove(channelId);
    }

    private static class GatewayInstanceManagerHolder {
        private static final GatewayInstanceManager INSTANCE = new GatewayInstanceManager();
    }
}

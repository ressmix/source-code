package com.tpvlog.im.gateway.tcp.common;

import redis.clients.jedis.Jedis;

public class JedisManager {

    private Jedis jedis = new Jedis("localhost");
    private JedisManager() {
    }

    public static JedisManager getInstance() {
        return Singleton.instance;
    }

    public Jedis getJedis() {
        return jedis;
    }

    static class Singleton {
        static JedisManager instance = new JedisManager();
    }
}

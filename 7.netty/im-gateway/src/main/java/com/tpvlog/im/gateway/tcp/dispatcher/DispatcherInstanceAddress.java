package com.tpvlog.im.gateway.tcp.dispatcher;

/**
 * 分发系统实例地址
 */
public class DispatcherInstanceAddress {
    private final String host;
    private final String ip;
    private final int port;

    public DispatcherInstanceAddress(String host, String ip, int port) {
        this.host = host;
        this.ip = ip;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }
}

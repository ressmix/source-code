package com.tpvlog.netty;

import io.netty.util.concurrent.FastThreadLocal;
import io.netty.util.concurrent.FastThreadLocalThread;

public class FastThreadLocalTest {
    private static final FastThreadLocal<String> THREAD_NAME_LOCAL = new FastThreadLocal<>();
    private static final FastThreadLocal<ThreadLocalTest.TradeOrder> TRADE_THREAD_LOCAL = new FastThreadLocal<>();

    public static void main(String[] args) {
        for (int i = 0; i < 2; i++) {
            int tradeId = i;
            new FastThreadLocalThread(new Runnable() {
                @Override
                public void run() {
                    ThreadLocalTest.TradeOrder tradeOrder = new ThreadLocalTest.TradeOrder(tradeId, tradeId % 2 == 0 ? "已支付" : "未支付");
                    TRADE_THREAD_LOCAL.set(tradeOrder);
                    THREAD_NAME_LOCAL.set(Thread.currentThread().getName());
                    System.out.println("threadName: " + THREAD_NAME_LOCAL.get());
                    System.out.println("tradeOrder info：" + TRADE_THREAD_LOCAL.get());
                }
            }).start();
        }
    }

    static class TradeOrder {
        long id;
        String status;

        public TradeOrder(int id, String status) {
            this.id = id;
            this.status = status;
        }

        @Override
        public String toString() {
            return "id=" + id + ", status=" + status;
        }
    }
}

package com.tpvlog.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class Server2 {
    public static void main(String[] args) throws InterruptedException {
        // 1.创建一个ServerSocketChannel
        ChannelFactory channelFactory = new ReflectiveChannelFactory(NioServerSocketChannel.class);
        Channel channel = channelFactory.newChannel();
        // 2.初始化Pipeline
        channel.pipeline().addLast(new ChannelInitializer<Channel>() {
            @Override
            public void initChannel(final Channel ch) {
                System.out.println("Chanel:" +ch);
            }
        });
        // 3.创建EventLoopGroup
        EventLoopGroup mainGroup = new NioEventLoopGroup(2);
        // 4.注册Channel
        ChannelFuture future = mainGroup.register(channel);
        future.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                System.out.println("future:" + future);
            }
        });
        Thread.sleep(60 * 3600 * 1000);
    }
}
